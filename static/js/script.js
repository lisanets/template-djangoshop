var myMap;
var maps = []
var routing = null;


/**
* Иницилизация карты
* Заранее требуются глобальные переменные myMap и markers
*/

function initYmap() {

	$(".map_container, .contacts-block__map-container").each(function (i, e) {
		if (!$(e).length) return false;


		maps[i] = {
			map_id: $(e).attr('id'),
			markers: []
		};
		$(e).find('div').each(function (div_i, el) {

			maps[i].markers.push({
				lat: $(el).attr('data-lat').replace(/,/, '.'),
				lng: $(el).attr('data-lng').replace(/,/, '.'),
				popup: $(el).attr('data-popup'),
				message: $(el).attr('data-message')
			});
			$(el).remove();

		});

		// Иницилизируем карту
		myMap = new ymaps.Map(maps[i].map_id, {
			center: [maps[i].markers[0].lat, maps[i].markers[0].lng],
			zoom: 14,
			controls: [],
		}, {
				searchControlProvider: 'yandex#search'
			});

		// Создаём макет содержимого.
		var MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
			'<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
		);


		// Добавляем на карту маркеры
		for (var marker of maps[i].markers) {
			var placemark = new ymaps.Placemark([marker.lat, marker.lng], {
				hintContent: marker.message,
				balloonContent: marker.popup,
			}, {
					iconLayout: 'default#imageWithContent',
					iconImageHref: '/static/images/map_pin.png',
					iconImageSize: [60, 64],
					iconImageOffset: [-30, -64],
					iconContentOffset: [22, 22],
					iconContentLayout: MyIconContentLayout
				});

			myMap.geoObjects.add(placemark);
		}
		myMap.events.add('sizechange', function () {
			myMap.setBounds(myMap.geoObjects.getBounds(), { checkZoomRange: true });
		});
	});

}

ymaps.ready(function () { initYmap() })


$(window).load(function(){
	var $preloader = $('#page-preloader');
	$preloader.fadeOut(300);

	(function($){


		var width;
		var b = 1;
		var sum = 0;
		$('.list_link a').each(function (i, el) {
			var text = $(el).text();
			// Приводим ширину элементов к целочисленному значению
			var w = Math.ceil($(el).outerWidth(true)); // добавляем значение отступа справа    
			sum += w;    // Складываем ширину соседних элементов                
			if (sum > width) { // Ограничиваем ширину
				b++; //Считаем кол-во строк
				sum = w; // Приравниваем значение ширины строки к ширине первого элемента в строке
			}
			if (b == 1) { width = 830; } // значение ширины 1 строки
			if (b == 2) { width = 760; } // значение ширины 2 строки
			else { width = 830; } // значение ширины остальных строк
			if (b >= 3) { // Убеждаемся что строк более 3х
				$(el).addClass('hidden_link'); // добавляем класс на элементы, которые будем скрывать
			}
		});
		$('.hidden_link').wrapAll("<div class='new'></div>"); // делаем обертку для 3 и более строк
		$('div.new').after('<a href="" class="open_all_list_link"><span>Еще</span><svg role="img" width="8" height="5"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="static/images/sprite.svg#caret-bottom"></use></svg></a>'); // добавляем кнопку ЕЩЕ
		$('.new a:last-child').after('<a href="" class="close_all_list_link"><span>Скрыть</span><svg role="img" width="8" height="5"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="static/images/sprite.svg#caret-bottom"></use></svg></a>'); // добавляем кнопку скрыть

		// Действия при кнопке ЕЩЕ
		$('a.open_all_list_link').click(function (event) {
			$(".list_link").toggleClass('opened');
			$('div.new').slideDown( 0, function () {
				$('.close_all_list_link').css('display', 'inline-block');
				$(".list_link a.hidden_link").unwrap();
			});
			$(this).css('display', 'none');
			

			return false
		});

		// Действия при кнопке СКРЫТЬ
		$('a.close_all_list_link').click(function (event) {
			$(".list_link").toggleClass('opened');
			$(".list_link a.hidden_link").wrapAll('<div class="new"></div>');
			$('div.new').slideUp(function () {
				$('.open_all_list_link').css('display', 'inline-block');
				
			});
			$(this).css('display', 'none');

			return false
		});
		/*  Перебор категорий при ресайзе  */ 
		function categoriesResize(){
			if ($(window).width() < 1200) {
				$('.close_all_list_link, .open_all_list_link').css('display', 'none');
				$(".list_link .new a.hidden_link").unwrap();
			}
			else {
				$(".list_link > a.hidden_link").wrapAll('<div class="new"></div>');
				if (!$(".list_link").hasClass("opened"))
					$('.open_all_list_link').css('display', 'inline-block');
				else
					$('.close_all_list_link').css('display', 'inline-block');
			}
		}
		categoriesResize();
		$(window).resize(function(){
			categoriesResize();
		});


		/*     Формирование столбцов в подменю     */
		$(".sub-menu > li > a").hover(function(i, e){
			element = $(this).next();
			max_height = element.parents(".sub-menu").outerHeight(true);
			sum = 16;

			element.append('<div class="row"><div class="col-4"></div></div>');
			element.find('> a').each(function(index, elem){
				if (sum + $(elem).outerHeight(true) < max_height){
					sum += $(elem).outerHeight(true);
				}
				else{
					element.find('.row').append('<div class="col-4"></div>');
					sum = $(elem).outerHeight(true) + 16;
				}

				element.find(".col-4:last-child").append($(elem)[0].outerHTML)
				$(elem).remove();

			});
		});



		/* Открытие другой модалки из модалки */
		$(".open_other_modal").click(function (e) {
			e.preventDefault();
			$(this).parents(".custom_modal").toggleClass("opened");
			$($(this).data("target")).toggleClass("opened");
		});

		/*    Открытие/закрытие модалки    */
		$(".modal_close, i.modal_bg, .modal_trigger").click(function (e) {
			e.preventDefault();
			$("body, i.modal_bg, .custom_modal.opened").toggleClass('opened');
			$($(this).data("target")).toggleClass("opened");
		});
		$("body").on('click', '.order-table__header .delete-item', function (e) {
			e.preventDefault();
			$("body, i.modal_bg, .custom_modal.opened").toggleClass('opened');
			$($(this).data("target")).toggleClass("opened");
		});


		/*	Открытие/закрытие мобильного меню
		---------------------------------------*/
		$(".mobile-header .ps-burger").click(function(){
			if (!$(this).hasClass("opened")) {
				if (!$("body").hasClass("opened"))
					$("body").addClass('opened');
			}
			else
				$("body").removeClass('opened');

			$(this).toggleClass('opened');
			$(".mobile-menu").toggleClass('opened');
			$(".mobile-search-wrapper, .mobile-search-trigger").removeClass('opened');

			if ($(this).hasClass("opened")){
				$(".mobile-menu .sub-menu.opened").removeClass('opened');
				$(".mobile-menu.no-scroll, .mobile-menu .sub-menu.no-scroll").removeClass('no-scroll');
			}
				
		});
		
		/*	Открытие мобильного подменю
		---------------------------------------*/
		$(".mobile-menu a .open-child").click(function(e){
			e.preventDefault();
			$( $(this).parents(".sub-menu, .mobile-menu")[0] ).toggleClass('no-scroll');
			$($(this).parents(".has-children")[0]).find("> .sub-menu").toggleClass('opened');
		});

		/*	Закрытие мобильного подменю
		---------------------------------------*/
		$(".mobile-menu .step_back").click(function (e) {
			e.preventDefault();
			$($(this).parents(".sub-menu.no-scroll, .mobile-menu.no-scroll")[0]).toggleClass('no-scroll');
			$($(this).parents(".sub-menu.opened")[0]).toggleClass('opened');
		});

		/*	Открытие/закрытие мобильного поиска
		---------------------------------------*/
		$(".mobile-search-trigger").click(function () {
			if (!$(this).hasClass("opened")) {
				if (!$("body").hasClass("opened")) 
					$("body").addClass('opened');
			}
			else
				$("body").removeClass('opened');

			$(this).toggleClass('opened');
			$(".mobile-menu, .mobile-header .ps-burger, .mobile-menu .sub-menu.opened").removeClass('opened');
			$(".mobile-menu.no-scroll, .mobile-menu .sub-menu.no-scroll").removeClass('no-scroll');
			$(".mobile-search-wrapper").toggleClass('opened');
		});



		/*	Открытие/закрытие мобильных фильтров
		---------------------------------------*/
		$(".mobile_filters_trigger, .nav_close").click(function (e) {
			e.preventDefault();
			$(".filters, body, .nav_close").toggleClass('opened');
		});




		$(".sn-card-add .add_to_cart, .sn-card-add .add_to_favorites, .sn-card-add .compare").click(function(e){
			e.preventDefault();
		});



		/*	Слайдер доп товаров в карточке товара
		---------------------------------------*/
		$('.extra_products-slider .slider-body').each(function (i, e) {
			$parent = $(e).parents('.extra_products-slider');
			$(e).slick({
				slidesToShow: 4,
				slidesToScroll: 4,
				nextArrow: $parent.find('.next'),
				prevArrow: $parent.find('.prev'),
				responsive: [
					{
						breakpoint: 1200,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3,
						}
					},
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2,
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					},
				]
			});
		})

		/*	Слайдер доп товаров в карточке товара
		---------------------------------------*/
		$('.extra-collections-slider .slider-body').each(function (i, e) {
			$parent = $(e).parents('.extra-collections-slider');
			$(e).slick({
				slidesToShow: 3,
				slidesToScroll: 3,
				nextArrow: $parent.find('.next'),
				prevArrow: $parent.find('.prev'),
				responsive: [
					{
						breakpoint: 1200,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2,
						}
					},
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2,
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1,
						}
					},
				]
			});
		})


		/*  Слайдер в карточке товара  */
		$('.product-slider__container').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			asNavFor: '.product-slider__carousel',
			responsive: [
				{
					breakpoint: 768,
					settings: {
						dots: true
					}
				}
			]
		});
		$('.product-slider__carousel').slick({
			slidesToShow: 4,
			asNavFor: '.product-slider__container',
			arrows: false,
			focusOnSelect: true,
			
		});


		/*  Появление всплывашки добавления в корзину  */
		$("body").on('click', '.add_to_cart, .add-to-cart', function () {
			$(this).addClass("added");
			var $this_ob = $(this);
			setTimeout(function () { $this_ob.removeClass("added") }, 3000);
		});

		

		$(".add_to_favorites, .compare").click(function(){
			$(this).toggleClass("active");
		})

		/*   Обработка клика по числовому инпуту   */
		$(".product-amount__button").click(function () {
			var $input = $(this).parents('.product-amount').find('input');
			var val = +$input.val();

			if ($(this).hasClass('product-amount__button_minus')) $input.val(val - 1);
			else $input.val(val + 1);

			$(this).parents('.product-amount').find('input').change();
		});
		$(".product-amount input").on("change", function () {
			var val = +$(this).val();
			var min = $(this).attr('min');
			var max = $(this).attr('max');
			$(this).val((val > max) ? max : (val < min) ? min : val);
		});


		/*   Переключение внешнего вида блоков товаров   */
		$(".sort_parameters .layout_types button").click(function(){
			$(".sort_parameters .layout_types button.active").removeClass("active");
			$(this).addClass("active");
		});



		/*	Запуск range-слайдера
		---------------------------------------*/
		$(".range_slider_field").each(function(i, e){
			var $range = $(e).find('.range_slider');
			$(e).find('.range_slider_inputs .min_price').val( $range.data("from") );
			$(e).find('.range_slider_inputs .max_price').val( $range.data("to") );

			$range.ionRangeSlider();
			$range.on("change", function () {
				var $this = $(this),
					value = $this.prop("value").split(";");

				$(e).find(".range_slider_inputs .min_price").val(value[0]);
				$(e).find(".range_slider_inputs .max_price").val(value[1]);
				filterShowCounter(this);
			});


			$(e).find(".range_slider_inputs input").change(function (index, elem) {
				filterShowCounter(this);

				$range_data = $(this).parents(".range_slider_field").find('.range_slider').data("ionRangeSlider");
				$range_data.update({
					from: $(e).find(".range_slider_inputs .min_price").val(),
					to: $(e).find(".range_slider_inputs .max_price").val()
				});
			});
		});


		/*	Всплывашка в фильтрах
		---------------------------------------*/
		function filterShowCounter(this_ob) {
			var $filters = $(this_ob).parents(".filters");
			$('a.common_count').css('top', $(this_ob).offset().top - $filters.offset().top - 6);
			$('a.common_count').fadeIn(200);
			clearTimeout(window.labelTimeout);
			window.labelTimeout = setTimeout(function () {
				$('a.common_count').fadeOut(200);
			}, 5000);
		}

		$('.filters input[type="checkbox"]').each(function (index, el) {
			$(el).click(function (event) {
				var current = $('.filters input[type="checkbox"]:checked').length;

				if (current > 0) {

					$('a.common_count').fadeIn(200);
					clearTimeout(window.labelTimeout);
					window.labelTimeout = setTimeout(function () {
						$('a.common_count').fadeOut(200);
					}, 5000);
				} else {

					$('a.common_count').fadeOut(200);
				}
				if ($(el).prop('checked')) {
					$(el).next('label').find('span').addClass('active');
				} else {
					$(el).next('label').find('span').removeClass('active');
				}
			});
		});

		$('a.common_count').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
			return false;
		});


		$('.filters input[type="reset"]').click(function (event) {
			$('.filters').find('span').removeClass('active');
			$('a.common_count').fadeOut(200);
		});

		$('.filters label').each(function (index, el) {
			$(el).click(function (event) {
				var position = $(el).position();
				var $filters = $(el).parents(".filters");
				filterShowCounter(this);
			});
		});
		// $('.range_slider_inputs input').each(function (index, el) {
		// 	$(el).change(function (event) {
		// 		var position = $(el).position();
		// 		var $filters = $(el).parents(".filters");
		// 		$('a.common_count').css('top', $(el).offset().top - $filters.offset().top - 10);
		// 		console.log(1111)
		// 	});
		// });

		$('.filter>div').on('hide.bs.collapse', function () {
			$('a.common_count').fadeOut(200);
		});



		$(".modal-order").on('scroll', function(){
			if( $(window).width() < 1200 ){
				var offsetTop = $(".modal-order").scrollTop() + $(".modal-order").outerHeight() - $(".modal-order .order-table__total").outerHeight();
				$(".modal-order .order-table__total").css({ "top": offsetTop });
			}
		})


		/*  Удаление товара из корзины   */ 
		$(".order-table__row .delete-item").click(function(){
			var height = $(this).parents(".order-table__row_body").outerHeight();
			var value_top = parseFloat($(this).parents(".order-table").find(".order-table__total").css('top'));
			
			if ( $(this).parents(".modal-order").length > 0 ){
				$(this).parents(".order-table").find(".order-table__total").css({ "top": value_top - height });
				$(this).parents(".order-table__row_body").remove();
			}
			else{
				$(this).parents(".order-table__row_body").slideUp(function () {
					$(this).parents(".order-table").find(".order-table__total").css({ "top": value_top - height });
					$(this).remove();
				});
			}
		});



		$('.index-row_stock .wrapper').slick({
			infinite: false,
			centerMode: false,
			centerPadding: '0px',
			slidesToShow: 2,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						arrows: false,
						centerMode: true,
						centerPadding: '15%',
						adaptiveHeight: true,
						slidesToShow: 1
					}
				}
			]
		});

		/*    Удаление товара из корзины    */
		$(".remove_product").click(function (e) { 
			e.preventDefault();
			var $product_block = $(this).parents(".sn-card-add");
			$product_block.addClass("removing");
		});
		/*    Восстановление товара в корзине    */
		$(".restore_product").click(function (e) { 
			e.preventDefault();
			var $product_block = $(this).parents(".sn-card-add");
			$product_block.removeClass("removing");
		});

		$('form.sn-search-form input').focusin(function (event) {
			$('.sn-search-dropdown ul').slideDown();
		});

		$('form.sn-search-form input').focusout(function (event) {
			$('.sn-search-dropdown ul').slideUp();
		});
		
		$(".sn-search-dropdown > ul").overlayScrollbars({
			autoUpdate: true
		})















		/*   Lazy-load картинок   */
		$('img.lazy').each(function (index, el) {
			$(el).parent().addClass('lazy_wrap');
			$(el).lazy({
				afterLoad: function (element) {
					$(el).parent().removeClass('lazy_wrap');
				}
			});
		});
		$('.lazy_bg').lazy();


		/*	Скролл для блоков фильтра
		---------------------------------------*/
		$('.filters .scroll_content, .seo-scroller .seo-scroller__content').overlayScrollbars({
			overflowBehavior: {
				x: "hidden" 
			}
			
		});

		$(".modal-order .order-table__body").overlayScrollbars({
			autoUpdate: true
		})


		custom_resize();

	})(jQuery);
});

function custom_resize(){

	$('.content img').each(function (i, e) {
		var w_post_img = $(e).width();
		var h_post_img = w_post_img * 32 / 87;
		$(e).css('height', h_post_img);
	});

	$('.gallery a').each(function(i, e) {
		var w_gallery_img = $(e).width();
		var h_gallery_img = w_gallery_img / 1.5;
		$(e).css('height', h_gallery_img);
	});

	$('.gallery .item-thumbnail, .certificates .certificate-thumbnail').each(function(i, e) {
		var w_gallery_img = $(e).width();
		var h_gallery_img = w_gallery_img / 1.5;
		$(e).css('height', h_gallery_img);
	});

	$('a.page_block').each(function(i, e){
		var block_w = $(e).outerWidth();
		var block_h = block_w * 420 / 630;
		$(e).css('height', block_h);
	});
}
custom_resize();
$(window).resize( function(){custom_resize(); } );

/*     Обертка таблицы на текстовых    */
$('.content-text > table').prev('h3').addClass('for_table');
$(".content-text > table").wrap("<div class='table'><div class='table-responsive'></div></div>");
$('.content-text > .table').each(function(){
	$(this).prev('h3.for_table').prependTo($(this));
});

/*     Маска телефона    */
$('input[type="tel"]').mask("+7 (999) 999-99-99");