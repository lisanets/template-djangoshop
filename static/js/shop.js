//start preloader
function startPreloader() {
	$('#page-preloader').addClass('ajax');
}

//stop preloader
function stopPreloader() {
	$('#page-preloader').removeClass('ajax');
}

//get get query string 
function get_get_query(more = false) {
	var filters = $('form.filters').serialize();
    var page = $('.pagination li.active a').attr('data-page');
    var sort = $('div.order_by select').serialize();
	if (more)
		page = $('#show_more').attr('data-page');

	return filters + '&page=' + page + '&' + sort;
}

//execute filtering
function updateCatalog(event) {
	event.preventDefault();
	startPreloader();
	
	query_string = get_get_query();
	window.history.pushState(null, null, window.location.pathname + "?" + query_string);
	query_string += "&ajax=Y";
	$.ajax({
		url: window.location.pathname,
		type: 'GET', 
		dataType: 'json',
		data: query_string,
		success: function(data) {
			stopPreloader();
			loadData(event, data);
		},
		error: function() { alert('error'); stopPreloader(); }
	});
}

function loadData(event, data, more = false) {
	$('.common_count').fadeOut(200);  
	
	if(data['products'].length != 0) {
		$("div.paginate").remove();
		var html_products = data['products'];
		if (more)
			$('div.change_products').append(html_products);
		else
            $('div.change_products').html(html_products);
        console.log(html_products)
        $('.filterBlock').replaceWith(data['template_filters']);
        rangeSliderInit();
		$('.paginationBlock').html(data['pagination']);
		// $(".products__filters_quantity").text(data['count']);
		stopPreloader();
	} else $('.products-container').html('<div class="row change_products"><p>В данной категории товар временно отсутствует, однако появится в ближайшее время. Пока предлагаем вам ознакомиться с продукцией в других разделах.</p></div>');

	$('img.lazy').each(function(index, el) {
		$(el).lazy({

		});
	});
}

function loadDataNews(event, data, target) {
	if(data['objects'].length != 0) {
		var html_objects = "";
		$.each(data['objects'], function(key, value) {
			html_objects += value;
		});
		html_objects += data['pagination'];
		target.closest(".row").append(html_objects);
		target.parents(".parentMore").remove()
		stopPreloader();
	}
}

//function change page 
function changePage(event) {
	event.preventDefault();
	var target = $(event.target);
	$('.pagination li').removeClass('active');
	$(target).closest('li').addClass('active');
	updateCatalog(event, false);
	$('body,html').animate({scrollTop: 0}, 400);
}

//set csrf to data
function csrf(str) {
	var input = $('.csrf_token').find('input');
	return str + "&" + $(input).attr('name') + "=" + $(input).attr('value');
}



function scroll() {
	$("body").animate({"scrollTop":400}, "slow");
}




//submit ajax forms
function submitAjaxForm(event, funcCall) {
	
	event.preventDefault();
	$('.error_message').html('');
	if(event.target.tagName == "BUTTON" || event.target.tagName == "A" )
		var target = $(event.target).closest("form");
	else 
		var target = $(event.target);
	target.closest("form").addClass("lazy_wrap");
	var data = $(target).serialize();
	var url = $(target).attr('action');
	var method = $(target).attr('method');

	offForm($(target));

	$.ajax({
		url: url, 
		type: method,
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
			funcCall(event, data);
			target.closest("form").removeClass('lazy_wrap');
		},
		error: function() { 
			alert('error');
			target.closest("form").removeClass('lazy_wrap'); 
		}
	});

}

function showErrors(event, data) {
    if(event.target.tagName == "BUTTON" || event.target.tagName == "A")
        target = $(event.target).closest("form");
    else
        target = $(event.target);
    $(target).find('input').removeClass("is-invalid is-valid");
    $(target).find('input').addClass("is-valid");
    $(target).find('div.state').removeClass("valid-feedback invalid-feedback");
    $(target).find('div.state').text("Готово!");
    $(target).find('div.state').addClass("valid-feedback");
    
	$.each(data['fields'], function(key, value) {
        var input_error = $(target).find('input[name=' + key +  ']')
        input_error.removeClass("is-valid");
        input_error.addClass("is-invalid");
        var div_error = $(input_error.siblings('div'));
        $(div_error).addClass('invalid-feedback');
        div_error.text(value);
		$(target).find('textarea[name=' + key +  ']').addClass('error');
		// $.each(value, function(key1, value1) {
		// 	error_message += value1 + "</br>";
		// });
	});
	// var err = $(target).find('.error_message');
	
	// $(err).html(error_message);
	// $(err).addClass('active');
}

function showErrorsButton(target, data) {
	var error_message = "";
	$.each(data['errors'], function(key, value) {
		$(target).find('input[name=' + key +  ']').addClass('error');
		$.each(value, function(key1, value1) {
			error_message += value1 + "</br>";
		});
	});
	var err = $(target).find('.error_message');
	
	$(err).html(error_message);
	$(err).addClass('active');
}

function offForm(form) {
	$(form).find('input').attr('disabled', true);
	//$(form).animate({opacity:0.5}, 500);
}

function onForm(form) {
	$(form).find('input').attr('disabled', false);
	//$(form).animate({opacity:1.0}, 0);
}



// //call form
// $('#callForm, #consForm').on('submit', function(event) {
// 	submitAjaxForm(event, callBackForm)
// })


$('#discForm').on('submit', function(event) {
	submitAjaxForm(event, discBackForm)
})

function callBackForm(event, data) {
	if(event.target.tagName == "BUTTON")
		target = $(event.target).closest("form");
	else
        target = $(event.target);
    onForm(target);
	if(data['errors']) {
		showErrors(event, data);
		console.log('error');
    } else $(target).html('<h3 style="color:white;">Заявка отправлена. Наш менеджер свяжется с вами в ближайшее время</h3>');
    if(data['reload'])
        window.location = window.location;
}

function discBackForm(event, data) {
	if(event.target.tagName == "BUTTON")
		target = $(event.target).closest("form");
	else
		target = $(event.target);
	onForm(target);
	$(".error_message").removeClass('active');
	if(data['error'] != 0) {
		showErrors(event, data);

	} else $(target).html('<h3>Заявка отправлена. Наш менеджер свяжется с вами в ближайшее время</h3>');
}





//execute filtering
function showMore(event) {
	event.preventDefault();
	$('a.ajax_page').addClass('ajax');
	startPreloader();
	query_string = get_get_query(true);
	window.history.pushState(null, null, window.location.pathname + "?" + query_string);
	query_string += "&ajax=Y";
	$.ajax({
		url: window.location.pathname,
		type: 'GET', 
		dataType: 'json',
		data: query_string,
		success: function(data) {
			loadData(event, data, true);
		},
		error: function() { alert('error'); stopPreloader(); }
	});
}



function countProducts(event) {
		// $(".tooltip_count").fadeOut();
		query_string = get_get_query();
		query_string += "&ajax=Y";
		$.ajax({
			url: window.location.pathname,
			type: 'GET', 
			dataType: 'json',
			data: query_string,
			success: function(data) {
				if (data['count']>0) 
				{
					$('.common_count span').text("Показать "+data['count']+ " " +data['word_count'])
				}
				else
				{
					$('.common_count span').text("Товаров не найдено ")
				}

			},
			error: function() { alert('error');}
		});
}


function addProductInCart(event) {
    event.preventDefault();
    var form = $(event.target).closest('form');
    var data = form.serialize();
    var url = form.attr('action');
    // console.log(data);
	$.ajax({
		url: url,
		type: 'POST', 
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
            $('div.order-table').replaceWith(data['dropdown']);
            $('span.cart_count').text(data['count']);
            $('span.cart_total').text(data['total']);
			console.log('succes');
			console.log(data['dropdown'])

		},
		error: function() { alert('error'); stopPreloader(); }
		});
}

function deleteProduct(event) {
	event.preventDefault();
	var $target = $(event.target).closest("div.basket__card_wrapper");
	var product = $target.attr("data-id")
	$.ajax({
		url: "/api/addproduct/",
		type: 'POST', 
		dataType: 'json',
		data: csrf("type=delete&id="+product),
		success: function(data) {
			if (data['error'] == 0) {
				$target.closest('.basket__card').remove();
				$('.basket__result_price').text(data['sum'] + " руб.");
			}

		},
		error: function() { alert('error'); stopPreloader(); }
		});
}

function changeCountProduct(event) {
	event.preventDefault();

	var $target = $(event.target).closest("div.basket__card_wrapper");
	if ($(event.target).attr('class') == 'add')
		changeCount = 1
	else
		changeCount = -1
	var count = Number($target.find(".count").text())+ changeCount;
	if (count < 1)
		count = 1;
	else if (count > 999)
		count = 999;
	var product = $target.attr("data-id");
	$.ajax({
		url: "/api/addproductfromcheckout/",
		type: 'POST', 
		dataType: 'json',
		data: csrf("count="+count+"&item_id="+product),
		success: function(data) {
			console.log("success");
			if (data['error'] == 0) 
			{
				$target.find(".count").text(count);
				$target.find(".basket__card_calc__factor").text(count + " x");
				$target.find(".basket__card_calc__sum").text(data['sum_item'] + " руб.");
				$('.basket__result_price').text(data['result_price'] + " руб.");
			}
			else
				alert(data['message']);

		},
		error: function() { alert('error'); stopPreloader(); }
		});
}

function sendCreateOrder(event) {
	event.preventDefault();
	var target = $(event.target);
	var data = $(target).serialize();
	target.closest("form").addClass("lazy_wrap");
	console.log(data);
	var url = $(target).attr('action');
	// $(target).find(".error_message").detach();
	// $(target).find(".error").removeClass("error");
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
			if(data['errors']) {
                showErrors(event,data)
				// $.each(data['fields'], function(key, value) {
				// 	$("input[name=" + key + "][type!=hidden][disabled!=disabled]").addClass('is-invalid');
				// });
				scroll();

			} else  { 
                console.log("!!!!");
                $("#custom_modal-0").html(data['template']);
                $(".notification_block").css('margin','0');
                $('.cart_total').text("-");
                $('.cart_count').text("0");
                // window.location = data['redirect']; 
            }
		target.closest("form").removeClass("lazy_wrap");
		},
		error: function() { 
			alert('error');
			target.closest("form").removeClass("lazy_wrap"); }
	});
}

var AJAX_SEARCH_MUTEX = false;

function smartAjaxSearch(event) {

    var search = $.trim($(event.target).val());
    var url = $(event.target).closest('form').attr('url-api');
    if ($(event.target).closest('form').attr('data-attr') == "desc")
    	class_block = ".sn-search-dropdown"
    else
    	class_block = ".sn-search-dropdown"
    console.log(class_block);
	if(search.length == 0) { 
				AJAX_SEARCH_MUTEX = false; 
				$('#navbarNavDropdown').html('');
				 $(class_block + ' ul').slideUp(150);
				return true;
	}
	if(AJAX_SEARCH_MUTEX && AJAX_SEARCH_MUTEX != event) {
		AJAX_SEARCH_MUTEX = event;
		return true;
	} 
	AJAX_SEARCH_MUTEX = event;
	$.ajax({
		url: url,
		type: 'POST',
		dataType: 'json',
		data: csrf($(event.target).closest('form').serialize()),
		success: function(data) {
			 console.log(data)
			if(AJAX_SEARCH_MUTEX == event) { AJAX_SEARCH_MUTEX = false;}
			else { smartAjaxSearch(AJAX_SEARCH_MUTEX); return true; }
			var result_html = "";
			if(data['template']) {
				result_html = data['template']
				$(class_block).html(result_html);
				$(class_block + ' ul').slideDown();
			} else {
				$(class_block).html('');
				$(class_block + ' ul').slideUp(150);

			}
		},
		error: function() { alert('error'); }
	});
}

function resetFilter(event){

	window.location = window.location.pathname;
}


function get_parameters() {
	var colorProduct = $($('.color_item').find('input:checked').siblings('i')[0]).attr('data-id');
	var length = $('#lengthProduct').val();
	var count = $('#countProduct').val();
	if (!colorProduct) colorProduct = "";
	if (!length) length = "";
	if (!count) count = "";
	return "cid="+colorProduct+"&leng="+length+"&count="+count;
}




function calc(event) {
	$.ajax({
		url: window.location,
		type: 'GET', 
		dataType: 'json',
		data: get_parameters(),
		success: function(data) {
			if (data['error'] == 0) {
				$('#countPM b').text(data['countPM']);
				$('#countKM b').text(data['countKM']);
				$('#priceMS b').text(data['priceMS']);
				$('.tovar_itog b').text(data['sum']);
			}
			else
				alert(data['message']);

		},
		error: function() { alert('error'); stopPreloader(); }
		});
}





function connectCard(event) {
	var card = $(event.target).attr('data-id');
	$.ajax({
		url: "/api/connectcard/",
		type: 'POST', 
		dataType: 'json',
		data: csrf("card_id="+card),
		success: function(data) {
			if (data['error'] == 0) {
				console.log('success');
				calc();
			}
			else
				console.log('problem');

		},
		error: function() { alert('error'); stopPreloader(); }
		});
}

function addProductFromCart(event) {
    var form = $(event.target).closest('form');
    var data = form.serialize();
    var url = form.attr('action');

	$.ajax({
		url: url,
		type: 'POST', 
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
            console.log("success");
            form.find('.total-price-number').text(data['price']);
            console.log(data['total']);
            $('.cart-table__footer strong').text(data['total'] + " руб.");
            $('.order-table__total b').text(data['total'] + " руб.");

		},
		error: function() { alert('error'); stopPreloader(); }
		});
}

function addFavoriteItem(event) {

    if (event.target.tagName == "BUTTON")
        var target = $(event.target);
    else
        var target = $(event.target).closest('button');
    var product = target.attr('data-product');
    var url = target.attr('data-url');

	$.ajax({
		url: url,
		type: 'POST', 
		dataType: 'json',
		data: csrf('product='+product),
		success: function(data) {
            console.log(" !!!!! ");

		},
		error: function() { alert('error'); stopPreloader(); }
		});
}


function delProductFromCart(event) {
	console.log('!!!!!!!!!!!!!');
	var form = $(event.target).closest('form');
    var data = form.serialize();

    var height = $(this).parents(".order-table__row_body").outerHeight();
			var value_top = parseFloat($(this).parents(".order-table").find(".order-table__total").css('top'));
			
			if ( $(this).parents(".modal-order").length > 0 ){
				$(this).parents(".order-table").find(".order-table__total").css({ "top": value_top - height });
				$(this).parents(".order-table__row_body").remove();
			}
			else{
				$(this).parents(".order-table__row_body").slideUp(function () {
					$(this).parents(".order-table").find(".order-table__total").css({ "top": value_top - height });
					$(this).remove();
				});
			}
	$.ajax({
		url: "/api/shop/cart/delete/",
		type: 'POST', 
		dataType: 'json',
		data: csrf(data),
		success: function(data) {
            $('.cart-table__footer strong').text(data['total'] + " руб.");
            $('.order-table__total b').text(data['total'] + " руб.");
            $('span.cart_count').text(data['count']);
		},
		error: function() { alert('error'); stopPreloader(); }
		});
}

function Subscribe(event) {
    console.log('!!!!!');
	var url = $(event.target).attr("data-action");
	$.ajax({
		url: url,
		type: 'POST', 
		dataType: 'json',
		data: csrf(''),
		success: function(data) {
            $(event.target).text(data['message']);
		},
		error: function() { alert('error'); stopPreloader(); }
		});
}



$(window).on('load',function() {

	// //set events
    // $('form.filters').on('submit', updateCatalog);
    $('body').on('submit', 'form.filters', updateCatalog)
	$('body').on('click', '#reset_filter', resetFilter)
	// $('body').on('change', '#type_category', changeType);
	// $('.filters input').on('change', countProducts);



	// $('#base_search').on('click',baseSearch);
	// $('#base_map').on('click',baseSearchMap);

	// $('.trigger-link').on('click',vacancyInput);

	// $('#main_cats input').on('change', checkMainCat);
	// // $('.select2-selection').on('click', countProducts);
	// //activate filters
	// //2017$('#filters_form input[type=checkbox]').on('click', updateCatalog);
	// $('#sorting').on('change', updateCatalog);
	// $('button.add, button.reduce').on('click', changeCountProduct);

	$('form.orderForm').on('submit', sendCreateOrder);


	$('.sn-search-form input').on('input', smartAjaxSearch);

	$('body').on('click', '.pagination li a', changePage);
	$('body').on('click', '.add_to_cart, .add-to-cart', addProductInCart);
	// $('.add_to_cart, .add-to-cart').on('click', addProductInCart);
	
	$('div.sale_select input').on('change', connectCard);

	$('body').on('change', 'input.change-count', addProductFromCart);
    $('body').on('click', '.order-table__body div.delete-item', delProductFromCart);
    $('.add_to_favorites').on('click', addFavoriteItem)

    // $('#login_form').on('submit', function(event) {
    //     submitAjaxForm(event, callBackForm)
    // })

    // $('body').on('click', '.subscribe_button', Subscribe);

    // $('#register_form').on('submit', function(event) {
    //     submitAjaxForm(event, callBackForm)
    // })

    // $('#change_personal_data').on('submit', function(event) {
    //     submitAjaxForm(event, callBackForm)
    // })

    // $('#change_password').on('submit', function(event) {
    //     submitAjaxForm(event, callBackForm)
    // })
    
    $('#custom_modal-1').on('submit', function(event) {
            submitAjaxForm(event, callBackForm);
    })
	
	

	$('div.color_item input').on('change', calc);
	$('#lengthProduct, #countProduct').on('input', calc);
	
	
	$('body').on('click', '#show_more', showMore);
});

