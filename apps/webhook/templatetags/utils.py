from django import template
from django.template.defaultfilters import floatformat


register = template.Library()


@register.filter(name='format_date')
def format_date(value, format_string="%d.%m.%Y"):
    return value.strftime(format_string)


@register.filter(name='formatted_float')
def formatted_float(value):
    value = floatformat(value, arg=4)
    return str(value).replace(',', '.')
