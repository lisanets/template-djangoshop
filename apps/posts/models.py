from django.db import models
from django.core.urlresolvers import reverse
from django.utils import timezone

from filer.fields.image import FilerImageField
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor_uploader.fields import RichTextUploadingField

from apps.configuration.utils import HrefModel, unique_slugify
from apps.content.models import Content

from django.contrib.sitemaps import Sitemap


class Category(MPTTModel):
    active = models.BooleanField(verbose_name='Активность', default=True)
    parent = TreeForeignKey('self', verbose_name="Родительская категория",
                            related_name='childs', blank=True, null=True,
                            on_delete=models.CASCADE)
    title = models.CharField(verbose_name='Заголовок',
                             blank=False, max_length=200)
    slug = models.SlugField(verbose_name='Слаг', blank=False,
                            null=True, unique=True, max_length=100)
    thumbnail = FilerImageField(verbose_name="Минеатюра",
                                null=True, blank=True)
    description = RichTextUploadingField(verbose_name='Описание',
                                         blank=True, default='')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.slug:
            self.slug = unique_slugify(self.slug, Category)
        else:
            self.slug = unique_slugify(self.title, Category)
        super(Category, self).save(*args, **kwargs)
        HrefModel.set_object(self)

    def get_absolute_url(self):
        return reverse('category', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = 'категория'
        verbose_name_plural = 'Категории'


class SitemapCatPage(Sitemap):
    model = Category
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return self.model.objects.filter(active = True)

class Post(models.Model):
    active = models.BooleanField(verbose_name='Активность', default=True)
    title = models.CharField(verbose_name='Заголовок',
                             blank=False, max_length=200)
    slug = models.SlugField(verbose_name='URL', blank=False,
                            null=True, unique=True, max_length=100)
    description = models.TextField(verbose_name='Краткое описание',
                                   blank=True, null=False)
    category = models.ForeignKey(Category, verbose_name='Категория',
                                 related_name='posts', blank=True, null=True)
    published_date = models.DateTimeField(verbose_name='Дата публикации',
                                          default=timezone.now, blank=False)
    thumbnail = FilerImageField(verbose_name="Минеатюра",
                                null=True, blank=True)
    content = models.ForeignKey(Content, verbose_name="Контент", blank=True,
                                null=True, related_name="content_posts")

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.slug:
            self.slug = unique_slugify(self.slug, Post)
        else:
            self.slug = unique_slugify(self.title, Post)
        super(Post, self).save(*args, **kwargs)
        HrefModel.set_object(self)

    def get_absolute_url(self):
        return reverse('post', kwargs={
            'category_slug': self.category.slug,
            'slug': self.slug})

    class Meta:
        verbose_name = 'запись'
        verbose_name_plural = 'Записи'
        ordering = ['-published_date']


class SitemapPost(Sitemap):
    model = Post
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return self.model.objects.filter(active = True)
