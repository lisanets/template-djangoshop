from django.conf.urls import url
from .views import CategoryView, PostView


urlpatterns = [
    url(r'^category/(?P<slug>[\w-]+)/$', CategoryView.as_view(),
        name='category'),
    url(r'^(?P<category_slug>[\w-]+)/(?P<slug>[\w-]+)/$', PostView.as_view(),
        name='post'),
]
