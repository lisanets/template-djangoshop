from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import TemplateView, ListView
from django.views.generic.detail import DetailView

from apps.catalog.models import Product
from apps.posts.models import Post, Category
from .models import Page, Offer


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hit_products'] = Product.objects.filter(
            active=True, hit=True).order_by('?')[0:4]
        context['new_products'] = Product.objects.filter(
            active=True, new=True).order_by('?')[0:4]
        context['sale_products'] = Product.objects.filter(
            active=True, sale=True).order_by('?')[0:4]
        try:
            news = Category.objects.filter(slug="news").first()
        except:
            news = Category.objects.filter(
                category__title__icontains="новости").first()
        
        context['collections'] = Product.objects.filter(type_obj = "collection", new = True)

        if news:
            context['news'] = Post.objects.filter(category=news)[:4]
            context['cat_news'] = news.get_absolute_url()
        else:
            context['news'] = []
            context['cat_news'] = ""
        return context


class PageView(DetailView):
    context_object_name = 'page'
    model = Page
    template_name = 'pages/page.html'


class OffersView(ListView):
    model = Offer
    context_object_name = 'offers'
    template_name = 'pages/offers.html'
    try:
        queryset = Offer.objects.filter(active=True)
    except ObjectDoesNotExist:
        queryset = False

    def get_context_data(self, **kwargs):
        context = super(OffersView, self).get_context_data(**kwargs)
        context['object'] = Page.objects.get(template=1)
        return context


class OfferView(DetailView):
    context_object_name = 'offer'
    model = Offer
    template_name = 'pages/offer.html'


class ContactsPage(TemplateView):
    context_object_name = 'contacts'
    template_name = 'pages/contacts.html'

    def get_context_data(self, **kwargs):
        context = super(ContactsPage, self).get_context_data(**kwargs)
        context['object'] = Page.objects.get(template=2)
        return context
