import os
import re

import requests
from django.core.management.base import BaseCommand
from django.core.files import File
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from xml.etree import ElementTree as ET
from slugify import slugify
from filer.models import Image

from system import settings
from ...models import (Category, Product, ProductGallery, AttributesGroup,
                       AttributeValue, Attribute, Option)


class Command(BaseCommand):
    help = 'Парсер с сайта gifts.ru'

    def __init__(self):
        super(Command, self).__init__()
        url_api = 'http://8577_xmlexport:8577_xmlexport@api2.gifts.ru/'
        self.product_counter = 0
        self.url_tree = url_api + 'export/v2/catalogue/tree.xml'
        self.url_filters = url_api + 'export/v2/catalogue/filters.xml'
        self.url_product = url_api + 'export/v2/catalogue/product.xml'
        self.url_slock = url_api + 'export/v2/catalogue/slock.xml'
        self.url_gifts = 'https://gifts.ru/'
        self.url_files = 'http://files.giftsoffer.ru/reviewer/'
        self.requires_headers = {
            "Accept-Language": "ru,en;q=0.8",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) " +
                          "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/" +
                          "61.0.3163.100 YaBrowser/17.10.1.1067 (beta) " +
                          "Yowser/2.5 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;" +
                      "q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Referer": "http://www.yandex.ru/clck/jsredir?from=yandex.ru;" +
                       "suggest;browser&text=",
            "Connection": "keep-alive",
            "Pragma": "no-cache"
        }

    def handle(self, *args, **options):
        self.search_attributes()
        self.set_products()
        self.search_categories()

    def search_categories(self):
        document = self.get_xml(self.url_tree)

        categories = document.find('page').findall('page')
        for category in categories:
            category_id = category.find('page_id').text
            category_title = category.find('name').text
            self.set_category(category_id, category_title)
            print('\n\n------------------------------')
            print(category_id)
            print(category_title)
            print('------------------------------\n\n')

            sub_categories = category.findall('page')
            for sub_category in sub_categories:
                sub_category_id = sub_category.find('page_id').text
                sub_category_title = sub_category.find('name').text
                print('\n\n\t------------------------------')
                print('\t' + sub_category_id)
                print('\t' + sub_category_title)
                self.set_category(sub_category_id, sub_category_title,
                                  category_id, sub_category)
                print('\t------------------------------\n\n')

    def set_category(self, unloading_id, title, parent_id=None, page=None):
        try:
            main_category = Category.objects.get(title='Сувениры')
        except ObjectDoesNotExist:
            main_category = Category.objects.create(title='Сувениры',
                                                    slug=slugify('Сувениры'))
        if parent_id:
            try:
                main_category = Category.objects.get(unloading_id=parent_id)
            except ObjectDoesNotExist:
                pass

        fields = ({'unloading_id': unloading_id, 'title': title,
                   'parent': main_category, 'slug': slugify(title)})
        obj, created = Category.objects.update_or_create(
            unloading_id=unloading_id, defaults=fields)

        # Привязка товаров к категории
        if page:
            products = page.findall('product')
            for item in products:
                try:
                    product_id = item.find('product').text
                    product = Product.objects.get(unloading_id=product_id)
                    product.category = obj
                    product.save()
                    if product.parent:
                        product.parent.category = obj
                        product.parent.save()
                    print('\t\tТовар привязан к категории: ' + product_id)
                except ObjectDoesNotExist:
                    print('Товар не найден: ' + product_id)

    def search_attributes(self):
        document = self.get_xml(self.url_filters)
        body = document.find('filtertypes')

        groups = body.findall('filtertype')
        for group in groups:
            group_id = group.find('filtertypeid').text
            group_title = group.find('filtertypename').text
            self.set_attribute_group(group_id, group_title)
            print('\n\n******************************')
            print(group_id)
            print(group_title)
            print('******************************\n\n')

            filters = group.find('filters').findall('filter')
            for filter_item in filters:
                attribute_id = filter_item.find('filterid').text
                attribute_title = filter_item.find('filtername').text
                self.set_attribute_value(attribute_id, attribute_title)
                print('\n\n\t******************************')
                print('\t' + attribute_id)
                print('\t' + attribute_title)
                print('\t******************************\n\n')

    def set_attribute_group(self, unloading_id, title):
        fields = ({'unloading_id': unloading_id, 'title': title,
                   'type_attr': 'checkbox', 'show': True})
        AttributesGroup.objects.update_or_create(
            unloading_id=unloading_id, defaults=fields)

    def set_attribute_value(self, unloading_id, title):
        fields = ({'unloading_id': unloading_id, 'title': title})
        AttributeValue.objects.update_or_create(
            unloading_id=unloading_id, defaults=fields)

    def set_products(self):
        document = self.get_xml(self.url_product)

        products = document.findall('product')
        for product in products:
            product_id = product.find('product_id').text
            product_code = product.find('code').text
            product_title = product.find('name').text
            product_thumbnail = self.url_files + \
                product.find('big_image').get('src')
            product_price = product.find('price').find('price').text
            product_content = product.find('content').text

            self.product_counter += 1
            print('\n\n<<<<<<<<<<' + str(self.product_counter) + '>>>>>>>>>>')
            print('id: ' + product_id)
            print('Артикул: ' + product_code)
            print('Заголовок: ' + product_title)
            print('Минеатюра: ' + product_thumbnail)
            print('Характеристики: ' + product_content)

            fields = ({
                'unloading_id': product_id,
                'code': product_code,
                'title': product_title,
                'price': product_price,
                'description': product_content
            })
            obj, created = Product.objects.update_or_create(
                unloading_id=product_id, defaults=fields)

            if not obj.thumbnail:
                self.set_thumbnail(obj, product_thumbnail)

            try:
                collection_id = product.find('group').text
                collection_title_re = re.search(
                    r'(?P<text>\w.+),.+?$', product_title)
                fields = ({
                    'unloading_id': collection_id,
                    'title': collection_title_re.group('text'),
                })
                collection = Product.objects.update_or_create(
                    unloading_id=collection_id, defaults=fields)
                if not collection[0].thumbnail:
                    collection[0].thumbnail = obj.thumbnail
                    collection[0].save()
                obj.parent = collection[0]
                obj.save()
            except:
                pass

            if not obj.product_gallery.all():
                gallery = product.findall('product_attachment')
                for item in gallery:
                    try:
                        image = self.url_files + item.find('image').text
                        ProductGallery.objects.create(
                            product=obj,
                            photo=self.set_gallery_image(image))
                        print('Изображение галереи: ' + image)
                    except AttributeError:
                        print('Тег изображения не найден')

            # Удаление старых и создание новых опций
            try:
                options = product.findall('product')
                obj.options.all().delete()
                for option in options:
                    option_id = option.find('product_id').text
                    option_title = option.find('name').text
                    option_fields = ({
                        'unloading_id': option_id,
                        'product': obj,
                        'title': option_title,
                        'price': option.find('price').find('price').text
                    })
                    Option.objects.update_or_create(
                        unloading_id=option_id, defaults=option_fields)
                    print('\tОпция товара: ' + option_title)
            except AttributeError:
                print('У товара отстствуют опции')

            # Удаление старых атрибутов и создание новых
            obj.product_attrbutes.all().delete()
            filters = product.find('filters').findall('filter')
            for item in filters:
                value_id = item.find('filterid').text
                group_id = item.find('filtertypeid').text
                print('\tСоздан фильтр: ' + value_id + ':' + group_id)
                attribute_group = AttributesGroup.objects.get(
                    unloading_id=group_id)
                attribute_value = AttributeValue.objects.get(
                    unloading_id=value_id)
                Attribute.objects.create(
                    product=obj, group=attribute_group,
                    value=attribute_value)
            print('<<<<<<<<<<' + str(self.product_counter) + '>>>>>>>>>>\n\n')

    def set_thumbnail(self, model_object, image):
        name = image[image.rfind('/') + 1:]
        path = name
        url = image
        image = self.create_image(path, url)
        model_object.thumbnail = image
        model_object.save()
        print(model_object.thumbnail)

    def set_gallery_image(self, image):
        name = image[image.rfind('/') + 1:]
        path = name
        url = image
        image = self.create_image(path, url)
        return image

    def create_image(self, file_path, url):
        full_path = open(settings.MEDIA_ROOT +
                         "/thumbnails/" + file_path, 'wb')
        request = requests.get(url, headers=self.requires_headers)
        full_path.write(request.content)
        full_path.close()
        full_path = open(
            settings.MEDIA_ROOT + "/thumbnails/" + file_path, 'rb')
        file_obj = File(full_path, name="/thumbnails/" + file_path)
        user = User.objects.get(username='Admin')

        fields = ({'original_filename': "/thumbnails/" + file_path,
                   'owner': user, 'file': file_obj})
        obj, created = Image.objects.get_or_create(
            original_filename="/thumbnails/" + file_path, defaults=fields)
        full_path.close()

        return obj

    def get_xml(self, url):
        """
        Получение xml документа по url
        """
        file_url = url
        xml = os.path.join(settings.BASE_DIR, '1c/import_gifts.xml')

        with open(os.path.join(xml), 'wb') as out_stream:
            req = requests.get(file_url, stream=True)
            for chunk in req.iter_content(1024):  # Куски по 1 КБ
                out_stream.write(chunk)

        return ET.parse(xml).getroot()
