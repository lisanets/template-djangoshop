import scandir

import re
import os
import base64
import traceback
import shutil

from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from django.contrib.auth.models import User
from lxml import etree
from slugify import slugify
from filer.models import Image

from system import settings
from apps.catalog import models as catalog_models

import gc


REGULAR_FOR_NAME_IMPORT_FILE = r'^prise_big\.xml$'
REGULAR_FOR_NAME_OFFER_FILE = r'^offers\.xml$'


def generate_unique_slug(name, model):
    slug = slugify(name)
    count = 0
    while True:
        try:
            prod = model.objects.get(slug=slug)
            slug = slug + "_" + str(count)
            count += 1
        except Exception:
            break
    return slug


class Command(BaseCommand):
    SCAN_FILES = None
    # FILE_PATH = settings.BASE_DIR + '../../1c/'
    FILE_PATH = '/var/www/vhosts/red35.ru/1c/'
    CAT_PARENT = {}
    CAT_COUNT_UPDATE = 0
    CAT_COUNT_CREATE = 0
    PRODUCT_COUNT_CREATE = 0
    PRODUCT_COUNT_UPDATE = 0
    IMPORT_FILR = None
    NAMESPACE = ""

    def handle(self, *args, **options):
        document = self.is_exist_files()
        if not document:
            self.stderr.write("Файлы не найдены")
            return
        
        # Импорт атрибутов
        self.stdout.write("Импортирование атрибутов")
        for e, item in self.get_iter_items(self.NAMESPACE + "Свойство"):
            if item.find('ТипЗначений') is not None and item.find('ТипЗначений').text == 'Справочник':
                item_title = item.find('Наименование').text
                item_id = item.find('ИД').text

                if item.find('ВариантыЗначений').find('Справочник') is not None:
                    try:
                        catalog_models.AttributesGroup.objects.get(unloading_id=item_id)
                    except:
                        catalog_models.AttributesGroup.objects.create(unloading_id=item_id,title=item_title)
                    print(item.find('Наименование').text)
                    for _item in item.find('ВариантыЗначений').findall('Справочник'):
                        _item_id = _item.find('ИДЗначения').text
                        item_name = _item.find('Значение').text
                        if _item_id and item_name:
                            try:
                                catalog_models.AttributeValue.objects.get(unloading_id=_item_id)
                            except:
                                catalog_models.AttributeValue.objects.create(unloading_id=_item_id, title=item_name)
            item.clear()
            while item.getprevious() is not None:
                del item.getparent()[0]
            del item
        gc.collect()

        catalog_models.Category.objects.update(active = False)
        catalog_models.Product.objects.filter(type_obj="collection").update(active = False)

        for e, groups in self.get_iter_items(self.NAMESPACE + "Группа"):
            self.import_group(groups)
            del groups
        gc.collect()
        
      

        catalog_models.Product.objects.filter(type_obj="product").update(active = False)
        # Импорт товаров
        self.stdout.write("Импортирование товаров")
        count = 0
        products = self.get_iter_items(self.NAMESPACE + "Товар")
        for e, product in products:
            self.import_product(product)
            count += 1
            product.clear()
            while product.getprevious() is not None:
                del product.getparent()[0]
            del product
        gc.collect()
        
        for cat in catalog_models.Category.objects.all():
            if not cat.get_products(cat):
                cat.active = False
                cat.save()

        return
        # self.import_warehouses()
        # self.import_offers()

    def is_exist_files(self):
        files = scandir.scandir(self.FILE_PATH)
        self.SCAN_FILES = files
        b = False
        for s in files:
            b = True
            break
        return b

    def get_name_offers_file(self):
        for f in scandir.scandir(self.FILE_PATH):
            if re.match(REGULAR_FOR_NAME_OFFER_FILE, f.name):
                return f.name
        return False

    def get_offers_items(self):
        file_name = self.get_name_offers_file()
        if file_name:
            return etree.iterparse(self.FILE_PATH + file_name, tag=self.NAMESPACE + u"Предложение",  events=["end", ])
        else:
            return False

    def import_offer(self, offer):
        id_pr = offer.find(self.NAMESPACE + u"ИД").text
        try:
            product = catalog_models.Product.objects.get(id_1c=id_pr)
        except:
            pass

        # find price
        try:
            price = float(offer.find(self.NAMESPACE + u"Цены").find(self.NAMESPACE +
                                                                    u"Цена").find(self.NAMESPACE + u"ЦенаЗаЕдиницу").text.strip(" \n\t"))
        except:
            self.stderr.write(u"Проблема в импорте цены - " + id_pr)
            return
        if price < product.get_price():
            product.old_price = product.price
            product.count_imports = 0
        elif price > product.get_price():
            product.old_price = 0
            product.count_imports = 0
        elif product.get_price() == price and product.get_old_price() != 0:
            if catalog_models.Settings.get_count_days_discount() > product.get_count_imports():
                product.count_imports += 1
            else:
                product.old_price = 0
                product.count_imports = 0
        product.price = price

        # count product on shops
        count = 0
        product.stocks.all().delete()
        product.count = 50
        """
        sklads = offer.findall(self.NAMESPACE + "Склад")
        for sk in sklads:
            id_warehouse = sk.get("ИДСклада").strip(" \n\t")
            #get warehouse
            warehouse = catalog_models.Warehouse.objects.get(active = True, id_1c = id_warehouse)
            warehouse_count = float(sk.get("КоличествоНаСкладе"))
            count += warehouse_count
            catalog_models.StockWarehouse.objects.create(product = product, count = warehouse_count, warehouse = warehouse)
        product.count = count
        """
        # set old price

        self.stdout.write(u"Импортирована цена для - " + id_pr)
        product.save()

    def import_offers(self):
        offers = self.get_offers_items()
        if offers:
            for o in offers:
                self.import_offer(o[1])

    def get_name_import_file(self):
        for f in scandir.scandir(self.FILE_PATH):
            if re.match(REGULAR_FOR_NAME_IMPORT_FILE, f.name):
                print(f.name)
                return f.name
        return False

    def get_iter_items(self, tag):
        import_file = self.get_name_import_file()
        if import_file:
            return etree.iterparse(self.FILE_PATH + import_file, tag=tag, events=["end", ])
        else:
            return []

    def import_group(self, group, parent=None):
        try:
            id_1c = group.find("ИД").text.strip(" \n\t")
            category_name = group.find(
                self.NAMESPACE + "Наименование").text.strip(" \n\t")
        except:
            self.stderr.write("Проблема с информацией в категории")
            traceback.print_exc()
            return

        if not category_name == "ВЫГРУЗКА НА САЙТ":
            try:
                variative = group.find(self.NAMESPACE + "Вариативный").text.strip(" \n\t")
                if variative == "Да":
                    print("\n\n\n","Коллекция",'\n\n\n')
                    variative = True
                else:
                    variative = False
            except:
                variative = False

            collection = category_name.find("Коллекция") 

            

            if variative or collection > -1:
                print("\n\nВариативный товар: " + category_name + "\n\n")
                try:
                    image_base64 = group.find(u'Картинка').find(
                        u'base64Binary').text.strip(" \t\n")
                except:
                    image_base64 = None
                

                if collection > -1:
                    type_obj = "collection"
                    category_name = category_name.replace("Коллекция ", "")
                else:
                    type_obj = "product"
                 # Создание и обновление категории
                fields = ({
                    'unloading_id': id_1c,'type_obj':type_obj, 
                    'title': category_name, 'slug': slugify(category_name),
                    'category': parent, 'price': 0,
                    'old_price': 0,
                    'active': True
                })

                try:
                    product, created = catalog_models.Product.objects.update_or_create(
                    unloading_id=id_1c, defaults=fields)
                    self.set_thumbnail(product, image_base64)
                except:
                    pass
                return
                



            # Создание или обновление категории товаров
            fields = ({
                'unloading_id': id_1c,
                'title': category_name,
                'slug': slugify(category_name),
                'parent': parent,
                'active': True
            })

            category, created = catalog_models.Category.objects\
            .update_or_create(unloading_id=id_1c, defaults=fields)



            category.save()
            tab = ""
            tab_parent = category.parent
            while tab_parent:
                tab += "--"
                tab_parent = tab_parent.parent
            
            print(tab, id_1c, category_name, category.parent)

            # Импорт картинки
            try:
                image_base64 = group.find(u'Картинка').find(
                    u'base64Binary').text.strip(" \t\n")
            except:
                image_base64 = None
            
            if image_base64:
                self.set_thumbnail(category, image_base64)

        else:
            category = None

        childs = group.find(self.NAMESPACE + "Группы")
        if childs is not None:
            for item in childs.findall(self.NAMESPACE + "Группа"):
                self.import_group(group = item,parent = category)
                del item

    def import_product(self, product, category = None, parent = None):
        product_xml = product
        try:
            id_1c = product.find(self.NAMESPACE + "ИД").text.strip(" \n\t")
            if category:
                cat = category
                group = cat.id
            else:
                group = product.find(self.NAMESPACE + "Группы").find(self.NAMESPACE + "ИД").text.strip(" \t\n")
                cats = catalog_models.Category.objects.filter(unloading_id=group)
                prods = catalog_models.Product.objects.filter(unloading_id=group)
                if cats:
                    cat = cats.first()
                elif prods:
                    cat = prods.first().category
                else:
                    self.stderr.write("Категория товара отсутствует")
                    return
            
            parents = catalog_models.Product.objects.filter(unloading_id=group)
            if not parent:
                if parents:
                    parent = parents.first()
                else:
                    parent = None


            product_name = product.find(
                self.NAMESPACE + "Наименование").text.strip(" \t\n")
            try:
                product_unit = product.find(
                self.NAMESPACE + "БазоваяЕдиница").get("НаименованиеПолное")
            except:
                product_unit = "шт"
            

            try:
                product_price = product.find(self.NAMESPACE + "Цена").text.strip(" \n\t").replace(",",".").replace(" ","")
                print("Цена " + product_price)
                product_price = float(product_price)
            except:
                product_price = 0

            product_count = 10

            try:
                product_desc = product.find(
                    self.NAMESPACE + "Описание").text.strip(" \t\n")
            except AttributeError:
                product_desc = ""
            try:
                product_code = product.find(
                    self.NAMESPACE + "Артикул").text.strip(" \n\t")
            except AttributeError:
                product_code = ""
            try:
                product_barcode = product.find(
                    self.NAMESPACE + "Штрихкод").text.strip(" \n\t")
            except AttributeError:
                product_barcode = ""
            try:
                image_base64 = product.find(u'Картинка').find(
                    u'base64Binary').text.strip(" \t\n")
            except Exception:
                image_base64 = None
        except Exception:
            self.stderr.write("Проблема с информацией в товаре")
            traceback.print_exc()
            return
        print(id_1c, product_name, cat)

        # Создание и обновление категории
        fields = ({
            'unloading_id': id_1c, 'code': product_code,
            'title': product_name, 'slug': slugify(product_name),
            'category': cat, 'price': product_price,
            'old_price': 0, 'count': product_count,
            'description': product_desc,
            'active': True, 'parent': parent
        })

        try:
            product, created = catalog_models.Product.objects.update_or_create(
            unloading_id=id_1c, defaults=fields)
        except:
            return

        self.set_attributes(product_xml, product)
        self.set_thumbnail(product, image_base64)

         
        try:
            characteristic = product_xml.find(self.NAMESPACE + "СтавкиНалогов").find(self.NAMESPACE + "СтавкаНалога").find(self.NAMESPACE + "Характеристики").findall(self.NAMESPACE + "Характеристика")
        except:
            characteristic = ""
        print("------------------------------")
        print(product_name)
        print("------------------------------")

        for char in characteristic:
            print("CHARACTERISTIC")
            self.import_product(char, cat, product)
            

    def set_attributes(self, product_xml, product):
        product.product_attrbutes.all().delete()
        print("attrs_import_1")
        if product.parent:
            try:
                attrs = product_xml.find(self.NAMESPACE + "СоставХарактеристики").findall(self.NAMESPACE + "Свойство")
                value_attrs = product_xml.find(self.NAMESPACE + "СоставХарактеристики").findall(self.NAMESPACE + "ЗначениеСвойство")
            except Exception:
                return
            print("attrs_import_2")
            for index, attr in enumerate(attrs):
                print("attrs_import_3")
                # id_1c = attr.find(self.NAMESPACE + "ИД").text.strip(" \t\n")
                try:
                    attribute_name = attr.find(self.NAMESPACE + "НаименованиеСвойства")\
                    .text.strip(" \t\n")
                    attribute_value = value_attrs[index].find(self.NAMESPACE + "НаименованиезначениеСвойства")\
                    .text.strip(" \t\n")
                except:
                    continue
                print("attrs_import_4")
                print("=====",attribute_name,"=====",attribute_value,"=====")
                group, created = catalog_models.AttributesGroup\
                    .objects.update_or_create(title=attribute_name)
                value, created = catalog_models.AttributeValue\
                    .objects.update_or_create(title=attribute_value)
                fields = ({'value': value,})
                attr, created = catalog_models.Attribute.objects.update_or_create(product=product,group=group,defaults = fields)
                print("Attribute - ",attr,"----",created)
                print("attrs_import_5")
                print("Атрибут для товара импортирован - ", product.unloading_id)
            del attrs
            del value_attrs
        else:
            try:
                attrs = product_xml.find(self.NAMESPACE + "ЗначенияСвойств").findall(self.NAMESPACE + "ЗначенияСвойства")
            except Exception:
                return
            print("attrs_import_2")
            for attr in attrs:
                print("attrs_import_3")
                # id_1c = attr.find(self.NAMESPACE + "ИД").text.strip(" \t\n")
                try:
                    attribute_name = attr.find(self.NAMESPACE + "Наименование")\
                    .text.strip(" \t\n")
                    attribute_value = attr.find(self.NAMESPACE + "Значение")\
                    .text.strip(" \t\n")
                except:
                    continue
                print("attrs_import_4")
                print("=====",attribute_name,"=====",attribute_value,"=====")
                group, created = catalog_models.AttributesGroup\
                    .objects.update_or_create(title=attribute_name)
                value, created = catalog_models.AttributeValue\
                    .objects.update_or_create(title=attribute_value)
                fields = ({'value': value,})
                attr, created = catalog_models.Attribute.objects.update_or_create(product=product,group=group,defaults = fields)
                print("Attribute - ",attr,"----",created)
                print("attrs_import_5")
                print("Атрибут для товара импортирован - ", product.unloading_id)
            del attrs
        del product
        gc.collect()


    def import_warehouses(self):
        file_name = self.get_name_offers_file()
        # deactivate all warehouses
        if not file_name:
            return False
        catalog_models.Warehouse.deactivate()
        for e, w in etree.iterparse(self.FILE_PATH + file_name, tag=self.NAMESPACE + u"Склады",  events=["end", ]):
            for s in w.findall(self.NAMESPACE + "Склад"):
                id_1c = s.find(self.NAMESPACE + "ИД").text.strip(" \t\n")
                name_warehouse = s.find(
                    self.NAMESPACE + "Наименование").text.strip(" \t\n")
                # get warehouse
                try:
                    warehouse = catalog_models.Warehouse.objects.get(
                        id_1c=id_1c)
                    warehouse.title_1c = name_warehouse
                    warehouse.active = True
                    if not warehouse.is_change_title():
                        warehouse.site_title = name_warehouse
                    warehouse.save()
                except:
                    self.stdout.write("Добавлен новый склад " + id_1c)
                    catalog_models.Warehouse.objects.create(
                        id_1c=id_1c,  title_1c=name_warehouse, site_title=name_warehouse)

    # def add_arguments(self, parser):
    #     parser.add_argument('domen')

    def set_thumbnail(self, instance, image_base64):
        if not instance.thumbnail and image_base64:
            path, name = self.get_image_base64(image_base64, instance)
            thumbnail = self.create_image(path, name)
            instance.thumbnail = thumbnail
            instance.save()
            print(instance.thumbnail)
            del thumbnail
            gc.collect()

    def set_gallery_image(self, image):
        name = image[image.rfind('/') + 1:]
        path = name
        url = image
        image = self.create_image(path, url)
        return image

    def get_image_base64(self, image_base64, product):
        path = os.path.join(settings.MEDIA_ROOT, 'products')
        try:
            os.mkdir(path)
        except FileExistsError:
            pass
        path = os.path.join(path, product.slug + '.png')
        name = product.slug + '.png'
        self.decb64img(image_base64, path)
        return path, name

    def create_image(self, path, name):
        full_path = open(path, 'rb')
        file_obj = File(full_path, name=name)
        user = User.objects.get(username='Admin')
        fields = ({'original_filename': name, 'owner': user, 'file': file_obj})
        obj, created = Image.objects.get_or_create(
            original_filename=name, defaults=fields)
        full_path.close()
        return obj

    def decb64img(self, str, path):
        file_obj = open(path, 'wb')
        file_obj.write(base64.b64decode(str))
        file_obj.close()
        return path
