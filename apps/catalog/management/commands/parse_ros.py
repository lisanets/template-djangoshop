import os
import json
import re

import requests
from django.core.management.base import BaseCommand
from django.core.files import File
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from xml.etree import ElementTree as ET
from slugify import slugify
from filer.models import Image

from system import settings
from ...models import (Category, Product, ProductGallery, AttributesGroup,
                       AttributeValue, Attribute)


class Command(BaseCommand):
    help = 'Получаем дерево каталога, со списком товаров'

    def __init__(self):
        super(Command, self).__init__()
        # Счетчик товаров
        self.product_counter = 0
        # Заголовки для get запросов
        self.requires_headers = {
            "Accept-Language": "ru,en;q=0.8",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) " +
                          "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/" +
                          "61.0.3163.100 YaBrowser/17.10.1.1067 (beta) " +
                          "Yowser/2.5 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;" +
                      "q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Referer": "http://www.yandex.ru/clck/jsredir?from=yandex.ru;" +
                       "suggest;browser&text=",
            "Connection": "keep-alive",
            "Pragma": "no-cache"
        }
        # Создание или обновление основной категории товаров
        fields = ({'unloading_id': 'ros-0', 'title': 'Кубки и награды',
                   'slug': slugify('Кубки и награды')})
        self.main = Category.objects.update_or_create(
            unloading_id='ros-0', defaults=fields)

    def handle(self, *args, **options):
        self.set_categories()
        self.set_products()

    def set_categories(self):
        categories = self.search_categories()
        for item in categories:
            category_id = self.get_id(item['id'])
            category_parent_id = self.get_id(item['parent_id'])
            category_name = item['name']

            try:
                parent = Category.objects.get(
                    unloading_id=category_parent_id)
            except ObjectDoesNotExist:
                parent = self.main[0]
            fields = ({'unloading_id': category_id, 'title': category_name,
                       'slug': slugify(category_name), 'parent': parent})
            Category.objects.update_or_create(
                unloading_id=category_id, defaults=fields)

            print('\n\n------------------------------')
            print('Идентификатор: ' + category_id)
            print('Родитель: ' + category_parent_id)
            print('Наименование: ' + category_name)
            print('------------------------------\n\n')

    def set_products(self):
        products = self.search_products()
        for item in products:
            product_name = item['name']
            options = item['offers']
            length = len(options)
            category = Category.objects.get(
                unloading_id=self.get_id(item['category']['id']))

            self.product_counter += 1
            print('\n\n<<<<<<<<<<' + str(self.product_counter) + '>>>>>>>>>>')
            print('Наименование: ' + product_name)
            print('Категория: ' + category.unloading_id)
            print('Количество опций: ' + str(length))

            # Общий товар коллекции, если опций больше олной
            if length > 1:
                fields = ({
                    'title': product_name,
                    'slug': slugify(product_name),
                    'category': category
                })
                collection, created = Product.objects.get_or_create(
                    title=product_name, defaults=fields)
            else:
                collection = None

            # Перебор опций товаров
            for option in options:
                # В данном случае относительно выгрузки, опции и есть товары
                option_id = option['id']
                option_name = option['name']
                option_base_price = option['base_price']
                option_price = option['price']
                option_image = option['image']
                option_properties = option['properties']

                fields = ({
                    'unloading_id': option_id,
                    'title': option_name,
                    'slug': slugify(option_name),
                    'price': option_base_price,
                    'category': category,
                    'parent': collection
                })
                product, created = Product.objects.update_or_create(
                    unloading_id=option_id, defaults=fields)

                if not product.thumbnail and option_image:
                    self.set_thumbnail(product, option_image)

                if collection:
                    if not collection.thumbnail:
                        collection.thumbnail = product.thumbnail
                        collection.save()

                # Вывод на консоль данные от опции
                print('\tИдентификатор опции: ' + option_id)
                print('\tНаименование опции: ' + option_name)
                print('\tБазовая цена опции: ' + option_base_price)
                print('\tЦена опции: ' + option_price)
                print('\t****************************************')

                # Свойства товаров. Свойства товоров выстпают как атрибуты
                product.options.all().delete()
                for prop in option_properties:
                    prop_name = prop['name']
                    prop_value = prop['value']
                    print('\t\tНаименование свойства: ' + prop_name)
                    print('\t\tЗначение свойства: ' + prop_value)
                    print('\t\t========================================')

                    # Группа атрибутов
                    fields = ({'title': prop_name, 'type_attr': 'checkbox',
                               'show': True})
                    attr_group = AttributesGroup.objects.update_or_create(
                        title=prop_name, defaults=fields)

                    # Значение атрибутов
                    attr_value = AttributeValue.objects.update_or_create(
                        title=prop_value, defaults=({'title': prop_value}))

                    # Атрибут товаров
                    Attribute.objects.create(
                        product=product, group=attr_group[0],
                        value=attr_value[0])
            print('<<<<<<<<<<' + str(self.product_counter) + '>>>>>>>>>>\n\n')

    def search_categories(self):
        """Получение выгрузки категорий товаров

        Returns:
            list -- Список категорий товаров
        """
        action = 'http://api.rossimvol.ru/catalog/'
        data = {
            'mode': 'categories',
            'token': self.get_token()
        }
        response = requests.get(action, params=data).content.decode()
        return json.loads(response)['result']

    def search_products(self):
        """Получение товаров из выгрузки

        Returns:
            list -- Список товаров
        """
        action = 'http://api.rossimvol.ru/catalog/'
        data = {
            'mode': 'products',
            'token': self.get_token()
        }
        response = requests.get(action, params=data).content.decode()
        return json.loads(response)['result']

    def set_thumbnail(self, model_object, image):
        name = image[image.rfind('/') + 1:]
        path = name
        url = image
        image = self.create_image(path, url)
        model_object.thumbnail = image
        model_object.save()
        print(model_object.thumbnail)

    def set_gallery_image(self, image):
        name = image[image.rfind('/') + 1:]
        path = name
        url = image
        image = self.create_image(path, url)
        return image

    def create_image(self, file_path, url):
        """Создание изображения в django-filer

        Arguments:
            file_path {str} -- Путь до файла в качетве его имени
            url {str} -- url до файла

        Returns:
            `Image` -- Объект модели изображения модуля django-filer
        """

        full_path = open(settings.MEDIA_ROOT +
                         "/thumbnails/" + file_path, 'wb')
        request = requests.get(url, headers=self.requires_headers)
        full_path.write(request.content)
        full_path.close()
        full_path = open(
            settings.MEDIA_ROOT + "/thumbnails/" + file_path, 'rb')
        file_obj = File(full_path, name="/thumbnails/" + file_path)
        user = User.objects.get(username='Admin')

        fields = ({'original_filename': "/thumbnails/" + file_path,
                   'owner': user, 'file': file_obj})
        obj, created = Image.objects.get_or_create(
            original_filename="/thumbnails/" + file_path, defaults=fields)
        full_path.close()

        return obj

    def get_id(self, obj_id):
        return 'ros-' + str(obj_id)

    def get_token(self):
        action = 'http://api.rossimvol.ru/token/'
        data = {
            'login': 'arnika35',
            'password': 'zvHJK0d'
        }
        response = requests.post(action, data=data).content.decode()
        return json.loads(response)['result']

    def get_xml(self, url):
        """
        Получение xml документа по url
        """
        file_url = url
        xml = os.path.join(settings.BASE_DIR, '1c/import.xml')

        with open(os.path.join(xml), 'wb') as out_stream:
            req = requests.get(file_url, stream=True)
            for chunk in req.iter_content(1024):  # Куски по 1 КБ
                out_stream.write(chunk)

        return ET.parse(xml).getroot()
