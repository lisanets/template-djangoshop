from django.core.management.base import BaseCommand
from django.core.files import File as dj_File
from django.contrib.auth.models import User
from filer.models import Image
from slugify import slugify
from django.utils.safestring import mark_safe

from system import settings
from apps.pages.models import Page
from apps.content.models import (Content, Text, Quote, TitleUnderline,
                            FileBlock, File, Files,
                            CertificatesBlock, Certificate, Certificates,
                            GalleryBlock, GalleryItem, Gallery
                        )
from apps.nav.models import Navigation
from apps.configuration.utils import HrefModel


# .mark_safe
class Command(BaseCommand):
    help = 'Создание стаднартных страниц'
    pages = [
        {
            'parent': None,
            'title': 'О компании'
        },
        {
            'parent': 'О компании',
            'title': 'Пример типовой текстовой страницы'
        },
        {
            'parent': None,
            'title': 'Новости'
        },
        {
            'parent': None,
            'title': 'Доставка'
        },
        {
            'parent': None,
            'title': 'Контакты'
        }
    ]

    content_wrapper = {
        'content': "Пример типовой текстовой страницы",
        'text': ['<h1>H1. Пример заголовка типовой текстовой страницы</h1>',
                '<p>В конце ноября, в оттепель, часов в девять утра, поезд Петербургско-Варшавской железной дороги на всех парах подходил к Петербургу. Было так сыро и туманно, что насилу рассвело. Ссылка в тексте, ховер для ссылки, посещённая ссылка.</p>',
                '<p>В одном из вагонов третьего класса, с рассвета, очутились друг против друга, у самого окна, два пассажира — оба люди молодые, оба почти налегке, оба не щегольски одетые, оба с довольно замечательными физиономиями и оба пожелавшие.</p>',
                '<h2>H2. Пример подзаголовка второго уровня типовой текстовой страницы в две строки и более</h2>',
                '<p>Один из них был небольшого роста, лет двадцати семи, курчавый и почти черноволосый, с серыми маленькими, но огненными глазами. Нос его был широк и сплюснут, лицо скулистое; тонкие губы беспрерывно складывались в какую-то наглую.</p>',
                '<h3>H3. Пример подзаголовка типовой текстовой страницы</h3>',
                '<p>Особенно приметна была в этом лице его мертвая бледность, придававшая всей физиономии молодого человека изможденный вид, несмотря на довольно крепкое сложение, и вместе с тем что-то страстное, до страдания.</p>'
            ],
        'Quote': 'Если б они оба знали один про другого, чем они особенно в эту минуту замечательны, то, конечно,\
                подивились бы, что случай так странно посадил их друг против друга в третьеклассном вагоне поезда.',
        'TitleUnderline': 'Подзаголовок с линией',
        'FileBlock': 'Пример блока с файлами',
        'File': ['category-img1.jpg', 'category-img2.jpg'],
        'GalleryBlock2': 'Пример блока галереи с 2 изображениями',
        'GalleryItem2': ['les1.jpg', 'les2.jpg'],
        'GalleryBlock3': 'Пример блока галереи с 3 изображениями',
        'GalleryItem3': ['les1.jpg', 'les2.jpg', 'les3.jpg'],
        'GalleryBlock4': 'Пример блока галереи с 4 изображениями',
        'GalleryItem4': ['les1.jpg', 'les2.jpg', 'les3.jpg', 'les4.jpg'],
    }

    def handle(self, *args, **options):
        
        # Добавление контанта
        content, created = Content.objects.update_or_create(
           title=self.content_wrapper['content']
           )
        
        # Добавление текстовых блоков
        for item in self.content_wrapper['text']:
            Text.objects.update_or_create(
                content=content, 
                text=mark_safe(item),
                sort=1
                )

        # Добавление блоков цитат
        Quote.objects.update_or_create(
                content=content, 
                text=self.content_wrapper['Quote']
                )

        # Добавление заголовка с линией
        TitleUnderline.objects.update_or_create(
                content=content, 
                text=self.content_wrapper['TitleUnderline']
                )
        
        # Добавление блока файлов
        fileblock, created = FileBlock.objects.update_or_create(
            title=self.content_wrapper['FileBlock']
            )

        # Добавление файлов в блок
        for item in self.content_wrapper['File']:
            File.objects.update_or_create(
                block=fileblock,
                obj=self.create_image(item),
                title='Пример прикрепленного документа'
                )

        # Добавление блока файлов в контент
        Files.objects.update_or_create(
            content=content,
            block=fileblock
        )
        # Добавление блока галереи 2 изображения
        gallery_block, created = GalleryBlock.objects.update_or_create(
            title=self.content_wrapper['GalleryBlock2'],
            type_show=6
            )

        # Добавление изображений в блок галереи 2 изображения
        for item in self.content_wrapper['GalleryItem2']:
            GalleryItem.objects.update_or_create(
                block=gallery_block,
                image=self.create_image(item),
                )

        # Добавление блока галереи в контент 2 изображения
        Gallery.objects.update_or_create(
            content=content,
            block=gallery_block
        )

        # Добавление блока галереи 3 изображения
        gallery_block, created = GalleryBlock.objects.update_or_create(
            title=self.content_wrapper['GalleryBlock3'],
            type_show=4
            )

        # Добавление изображений в блок галереи 3 изображения
        for item in self.content_wrapper['GalleryItem3']:
            GalleryItem.objects.update_or_create(
                block=gallery_block,
                image=self.create_image(item),
                )

        # Добавление блока галереи в контент 3 изображения
        Gallery.objects.update_or_create(
            content=content,
            block=gallery_block
        )

        # Добавление блока галереи 4 изображения
        gallery_block, created = GalleryBlock.objects.update_or_create(
            title=self.content_wrapper['GalleryBlock4'],
            type_show=3
            )

        # Добавление изображений в блок галереи 4 изображения
        for item in self.content_wrapper['GalleryItem4']:
            GalleryItem.objects.update_or_create(
                block=gallery_block,
                image=self.create_image(item),
                )

        # Добавление блока галереи в контент 4 изображения
        Gallery.objects.update_or_create(
            content=content,
            block=gallery_block
        )

        # Создание типовой текстовой страницы и навигации
        for item in self.pages:
            if item['parent']:
                parent = Page.objects.get(title=item['parent'])
                nav_parent, created = Navigation.objects.update_or_create(
                    title=str(parent),
                    object_href=HrefModel.objects.get(
                        object_path=HrefModel.generate_object_path(parent)
                        )
                    )
            else:
                parent = None
                nav_parent = None
            fields = ({'title': item['title'], 
                        'parent': parent,
                        'slug': slugify(item['title']),
                        'content': content
                        })

            page, created = Page.objects.update_or_create(
                title=item['title'], defaults=fields
                )

            Navigation.objects.update_or_create(
                    title=str(page),
                    parent=nav_parent,
                    object_href=HrefModel.objects.get(
                        object_path=HrefModel.generate_object_path(page)
                        )
                    )

            print('------------------------------')
            print('Добавлена страница '+item['title'])
            print('\tДобавлена навигация '+item['title'])
            print('------------------------------')

    # Функция загрузки файлов   
    def create_image(self, file_name):
        full_path = open(settings.BASE_DIR + "/static/images/" + file_name, 'rb')
        file_obj = dj_File(full_path, file_name)
        user = User.objects.get(username='Admin')

        fields = ({'original_filename': str(file_name),
                   'owner': user, 
                   'file': file_obj
                })

        obj, created = Image.objects.get_or_create(
            original_filename=file_name, defaults=fields)
        full_path.close()

        return obj