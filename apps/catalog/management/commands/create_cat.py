import os

from slugify import slugify
from django.core.management.base import BaseCommand
from django.core.files import File as dj_File
from django.contrib.auth.models import User
from filer.models import Image
from system import settings
from ...models import (Category, Product, ProductGallery, AttributesGroup,
                       AttributeValue, Attribute)


class Command(BaseCommand):
    help = 'Заполнение категорий и продуктов'

    def handle(self, *args, **options):
        
        for counter_cat in range(5):
            category, created = Category.objects.update_or_create(
                    active=True,
                    unloading_id='cat-'+str(counter_cat),
                    title='Тестовая категория '+str(counter_cat),
                    slug=slugify('Тестовая категория '+str(counter_cat)),
                    thumbnail=self.create_image('category-img1.jpg'),
                    description='Пример описания тестовой категории'
                )
            print('------------------------------')
            print('Категория {0} создана'.format(counter_cat))

            for counter_prod in range(5):
                prod, created = Product.objects.update_or_create(
                        title='Тестовый продукт {0} в категории {1}'.format(
                            counter_prod, counter_cat
                        ),
                        active=True,
                        unloading_id='prod-'+str(counter_prod),
                        slug=slugify('prod-{0}{1}'.format(
                            counter_cat, counter_prod
                        )),
                        category=category,
                        thumbnail=self.create_image('product_img1.jpg'),
                        description='Описание тестового продукта {0}\
                            в категории {1}'.format(
                            counter_prod, counter_cat
                        ),
                        price=int(1000+counter_prod),
                        count=100
                )
                print('\tПродукт {0} в категории {1} создан'.format(
                    counter_prod, counter_cat
                    )
                )

            print('------------------------------')
                
    # Функция загрузки файлов   
    def create_image(self, file_name):
        full_path = open(settings.BASE_DIR + "/static/images/" + file_name, 'rb')
        file_obj = dj_File(full_path, file_name)
        user = User.objects.get(username='Admin')

        fields = ({'original_filename': str(file_name),
                   'owner': user, 
                   'file': file_obj
                })

        obj, created = Image.objects.get_or_create(
            original_filename=file_name, defaults=fields)
        full_path.close()

        return obj