import re

from functools import lru_cache

from django.db import models
from django.core.urlresolvers import reverse
from django.template.loader import get_template
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor_uploader.fields import RichTextUploadingField
from filer.fields.image import FilerImageField
from django.db.models import Q

from apps.seo.models import SeoBase

from ..models import Category, Brand
from .. import models as catalog_models

from django.contrib.sitemaps import Sitemap


TYPES = (
    ("product", "Товар"),
    ("collection", "Коллекция"),
)

class Product(MPTTModel, SeoBase):
    title = models.CharField(verbose_name="Наименование", null=True,
                             blank=False, max_length=300)
    active = models.BooleanField(verbose_name="Активность", default=True)
    type_obj = models.CharField(verbose_name="Тип", choices=TYPES,
                                 default="product", blank=False,
                                 null=False, max_length=100)
    # related_products = models.ManyToManyField("self",verbose_name = "Похожие товары",symmetrical = False, blank = True)
    unloading_id = models.CharField('Идентификатор выгрузки', blank=True,
                                    default='', max_length=300)
    slug = models.SlugField(verbose_name='Слаг', blank=True, null=True,
                            unique=True, max_length=300)
    category = models.ForeignKey(Category, verbose_name='Категория',
                                 blank=True, null=True,
                                 related_name='products')
    parent = TreeForeignKey('self', verbose_name="Товар-родитель", blank=True,
                            null=True, related_name='product_child')
    brand = models.ForeignKey(Brand, verbose_name="Производитель", blank=True,
                              null=True, related_name="brand_products")
    thumbnail = FilerImageField(verbose_name="Фотография", null=True,
                                blank=True, related_name="product_thumbnail",
                                on_delete=models.SET_NULL)
    description = RichTextUploadingField(verbose_name="Описание товара",
                                         null=True, blank=True)
    hidden_description = RichTextUploadingField(verbose_name="Дополнительное описание (коллекция)",
                                         null=True, blank=True)
    code = models.CharField(verbose_name="Код товара", blank=True,
                            max_length=300)
    price = models.FloatField(verbose_name="Цена", default=0)
    old_price = models.FloatField(verbose_name="Старая цена", default=0)
    wholesale_price = models.FloatField(verbose_name="Оптовая цена", default=0)
    count = models.IntegerField(
        verbose_name="Количество", default=0, blank=True)
    new = models.BooleanField(verbose_name="Новинка", default=False)
    hit = models.BooleanField(verbose_name="Хит", default=False)
    sale = models.BooleanField(verbose_name="Распродажа", default=False)

    count_showing = models.IntegerField(verbose_name="Количество просмотров",
                                        default=0)

    def __str__(self):
        if self.type_obj == "collection":
            return self.title + " (Коллекция)"
        return self.title

    def get_price(self):
        if self.get_children():
            min_price = self.get_children().exclude(price = 0).order_by("price").first()
            if min_price:
                return min_price.price
        return self.price

    def get_absolute_url(self):
        if self.get_children() and not self.type_obj == "collection":
            return self.product_child.first().get_absolute_url()
        return reverse('product', kwargs={'slug': self.slug})
    
    def get_breadcrumbs(self):
        breadcrumbs = list()
        breadcrumbs.append(self)
        parent = self.parent
        while parent:
            breadcrumbs.append(parent)
            parent = parent.parent
        if self.category:
            return self.category.get_breadcrumbs() + breadcrumbs
        return breadcrumbs

    # @lru_cache(maxsize=64)
    def the_product_card(self):
        return get_template("catalog/product-card.html").render(
            {'product': self})

    def first_color(self):
        return self.colors.first()

    def set_viewed_products(self, request):
        """
        Запись просмотренных товаров в сессию
        """
        if not self.type_obj == "collection":
            try:
                if self.id not in request.session['viewed_products']:
                    request.session['viewed_products'].append(int(self.id))
            except KeyError:
                request.session['viewed_products'] = []
                request.session['viewed_products'].append(int(self.id))
            request.session.save()
        else:
            return

    def get_viewed_products(self, request):
        """
        Получение просмотренных товаров из сессии
        """
        if request.session.get('viewed_products', False):
            return Product.objects.filter(
                id__in=request.session['viewed_products']).exclude(
                    id=self.id)[0:12]
        return None
    
    def get_variations(self):
        if self.parent and self.parent.type_obj == "product":
            return self.parent.get_children()
        return []

    @classmethod
    # @lru_cache(maxsize=64)
    def get_filters(cls, products):
        data = {
            "attributes": cls._form_attributes_filter(products),
            "colors": cls._form_colors_filter(products),
            "new": products.filter(new=True).count(),
            "hit": products.filter(hit=True).count(),
            "sale": products.filter(sale=True).count(),
            "thumbnail": products.filter(thumbnail__gte=0).count(),
            "count": products.count()
        }
        return data

    @staticmethod
    def _form_attributes_filter(products):
        """Формирование фильтров по атрибутам относительно товаров.

        Arguments:
            products {QuerySet} -- Список с товарами

        Returns:
            dict -- Список групп атрибутов
        """

        groups = catalog_models.AttributesGroup.objects.filter(
            show=True, attributes__product__in=products).distinct()
        # .cache()
        attributes = list()
        for group in groups:
            attributes.append(group.search_attributes(products))
        return attributes

    @staticmethod
    def _form_colors_filter(products):
        """Формирование групп цветов относительно товаров.

        Arguments:
            products {QuerySet} -- Список товаров

        Returns:
            QuerySet -- Список групп цветов
        """

        groups = catalog_models.ColorGroup.objects.filter(
            color_values__colors__product__in=products).distinct()
        # .cache()
        return [{'title': item.title, 'name': 'cg_' + str(item.id),
                 'hex_color': item.hex_color,
                 'count': catalog_models.Product.objects.filter(
                     colors__value__group=item).count()} for item in groups]

    @staticmethod
    # @lru_cache(maxsize=64)
    def filter_products(products, request):
        data = {
            "attributes": [],
            "thumbnail": False,
            "new": False,
            "hit": False,
            "sale": False,
            "search": None,
        }

        attribute_filters = {}

        # Наполнение фильтров атрибутами
        if request.GET:
            for key, value in request.GET.items():
                attribute = re.match(
                    r"^ch_(?P<group>\d+)_(?P<value>\d+)$", key)

                # Атрибут строка
                if attribute:
                    attribute_decode = attribute.group("value")
                    if attribute_filters.get(int(attribute.group("group")),
                                             False):
                        attribute_filters[int(attribute.group("group"))]\
                            .append(str(attribute_decode))
                    else:
                        attribute_filters[int(attribute.group("group"))] = [
                            str(attribute_decode)]
                    data["attributes"].append(key)

            # Фильтрация товаров относительно атрибутов. Фильтрация ведется
            # по id групп и значений атрибутов.
            for attr in attribute_filters.items():
                products = products.filter(
                    product_attrbutes__group__id=attr[0],
                    product_attrbutes__value__in=attr[1], product_child=None)
                # .cache()

            # Проверка дополнительных полей
            if request.GET.get("thumbnail", "") == "on":
                products = products.filter(thumbnail__gte=0)
                data["thumbnail"] = True
            if request.GET.get("new", "") == "on":
                products = products.filter(new=True)
                data["new"] = True
            if request.GET.get("hit", "") == "on":
                products = products.filter(hit=True)
                data["hit"] = True
            if request.GET.get("sale", "") == "on":
                products = products.filter(discount_pr__gt=1)
                data["sale"] = True

        return products, data

    class Meta:
        verbose_name = "товар"
        verbose_name_plural = "Товары"


class ProductGallery(models.Model):
    """
    Галерея товаров
    """
    product = models.ForeignKey(Product, verbose_name="Товар", blank=False,
                                null=False, related_name="product_gallery")
    position = models.PositiveIntegerField(default=1, verbose_name="Позиция")
    photo = FilerImageField(verbose_name="Изображение", null=True, blank=True)
    alt = models.CharField(verbose_name="Alt", blank=True, default = "",
                            max_length=100)

    def __str__(self):
        return "Изображение: " + str(self.photo.name)

    class Meta:
        verbose_name = "изображение"
        verbose_name_plural = "Галерея"
        ordering = ['position']


class ColorSolution(models.Model):
    """
    Цветовые решения (для коллекции)
    """
    product = models.ForeignKey(Product, verbose_name="Товар", blank=False,
                                null=False, related_name="product_color_solution")
    title = models.CharField(verbose_name="Название", blank=True,
                            max_length=100)
    position = models.PositiveIntegerField(default=1, verbose_name="Позиция")
    photo = FilerImageField(verbose_name="Изображение", null=True, blank=True)

    def __str__(self):
        return "Изображение: " + self.title

    class Meta:
        verbose_name = "Цветовое решение"
        verbose_name_plural = "Цветовые решения"
        ordering = ['position']


class ProductSitemap(Sitemap):

    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Product.objects.filter(active=True)
