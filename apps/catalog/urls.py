from django.conf.urls import url

from .views import (
    CatalogList, CategoryJSONDetail, BrandsList, ProductDatail,
    ProductsJSONTemplate, SearchView
)


urlpatterns = [
    url(r'^catalog/$', CatalogList.as_view(), name='catalog'),
    url(r'^brands/$', BrandsList.as_view(), name="brands"),
    url(r'^products/$', ProductsJSONTemplate.as_view(), name="products"),

    url(r'^product-category/(?P<slug>[\w-]+)/$', CategoryJSONDetail.as_view(),
        name='product-category'),
    url(r'^product/(?P<slug>[\w-]+)/$', ProductDatail.as_view(),
        name='product'),

    # Поиск по товарам и услугам
    url(r'^api/catalog/search/$', SearchView.as_view(), name='search'),
]
