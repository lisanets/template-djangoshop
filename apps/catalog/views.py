import re

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, JsonResponse
from django.views.generic.list import ListView
from django.views.generic import View, TemplateView
from django.views.generic.detail import (
    DetailView, SingleObjectTemplateResponseMixin)
from django.template.loader import get_template

from apps.configuration.views import JSONDetailView, JSONResponseMixin
from apps.configuration.utils import pagination
from .models import Category, Brand, Product


class CatalogList(ListView):
    model = Category
    context_object_name = 'catalog'
    template_name = 'catalog/catalog.html'
    try:
        queryset = model.objects.filter(active=True, parent=None)
    except ObjectDoesNotExist:
        queryset = False


class CategoryJSONDetail(SingleObjectTemplateResponseMixin, JSONDetailView):
    model = Category
    context_object_name = 'product-category'
    template_name = 'catalog/category.html'

    def render_to_response(self, context, **response_kwargs):
        obj = context['object']
        request = self.request

        sort = self.request.GET.get('sort', False)
        products = self.model.get_products(obj,sort)
        context['filters'] = obj.get_filters(products)

        # Фильтрация товаров
        products, context['active'] = obj.filter_products(
            products, request)
        
        context['filtration_after'] = obj.get_after_filters(products)

    
        # Пагинация и кнопка показать еще
        if products:
            try:
                page = int(request.GET.get("page", 1))
            except:
                page = 1
            count_products = products.count()
            context['count'] = count_products

            if int(count_products) <= int(12):
                # context['more_button'] = ""
                context['pagination'] = ""
            else:
                pagin = pagination(products, page, 12)
                products = pagin.items
                context['pagination'] = \
                    get_template("catalog/includes/pagination.html").render(
                        {'PAGIN': pagin})
        else:
            context['count'] = 0

        if self.request.is_ajax():
            # Удаление лишних объектов
            del(context['object'], context['product-category'],
                context['view'])

            context['template_filters'] = get_template(
                "catalog/includes/filters.html").render(
                    {'filters': context['filters'], 'active': context['active'],
                     'filtration_after': context['filtration_after'], 'count':context['count']})

            # Сериализация требуемых объектов
            context['products'] = ''
            for item in products:
                context['products'] += \
                    '<div class="col-lg-4 col-sm-6 col-12">' \
                    + item.the_product_card() + '</div>'

            return self.render_to_json_response(context, **response_kwargs)
        else:
            context['products'] = products
            return super().render_to_response(context, **response_kwargs)


class BrandsList(ListView):
    model = Brand
    context_object_name = 'brands'
    template_name = 'catalog/brands.html'
    try:
        queryset = model.objects.filter(active=True).order_by('title')
    except ObjectDoesNotExist:
        queryset = False

    def get_context_data(self, *args, **kwargs):
        context = super(BrandsList, self).get_context_data(*args, **kwargs)
        brands = []
        group = []
        last_symbol = ''

        # Распределинеи производтелей по группам относительно первого символа
        for brand in self.queryset:
            symbol = brand.title[0]
            # if re.match(r'([0-9\_\@\-\=\.\,\\\/\'\"\`])', symbol):
            #     symbol = '0-9'
            # if re.match(r'([А-Яа-я])', symbol):
            #     symbol = 'А-Я'

            if last_symbol != symbol:
                group = []
                brands.append({'symbol': symbol, 'brands': group})

            group.append(brand)
            last_symbol = symbol

        context['brands'] = brands
        return context


class ProductDatail(DetailView):
    """Страница товара"""

    obj = None
    model = Product
    context_object_name = 'product'
    template_name = 'catalog/collection.html'

    def get_context_data(self, **kwargs):
        context = super(ProductDatail, self).get_context_data(**kwargs)

        product = context['object']
        context['children'] = product.get_children().order_by("category")
        if not product.get_children():

            # Добавление текущего товара в просмотренные товары
            product.set_viewed_products(self.request)

            # Просмотренные товары
            context['viewed_products'] = product.get_viewed_products(
                self.request)

            # Похожие товары. Товары подбираются относительно категорий в
            # которой находится текущий товар, и берутся товары только
            # последнего уровня вложенности

            # context['similar_products'] = Product.objects.filter(
            #     Q(product_child=None) & Q(parent=None) | Q(product_child=None),
            #     category__in=category.get_descendants(include_self=True),
            #     active=True).distinct().exclude(id=product.id)\
            #     .order_by("?")[:12]

            # Похожие товары. Товары подбираются относительно категорий в
            # которой находится текущий товар, и берутся товары только
            # последнего уровня вложенности
        category = product.category
        if product.type_obj == "collection":
            context['similar_products'] = Product.objects.filter(type_obj = "collection",
                category__in=category.get_descendants(include_self=True),
                active=True).distinct().exclude(id=product.id)\
                .order_by("?")[:12]
        else:
            context['similar_products'] = Product.objects.filter(
                Q(product_child=None) & Q(parent=None) | Q(product_child=None),
                category__in=category.get_descendants(include_self=True),
                active=True).distinct().exclude(id=product.id)\
                .order_by("?")[:12]


        return context

    def get_object(self, queryset=None):
        try:
            obj = super(ProductDatail, self).get_object(queryset=queryset)
            if not obj.get_children() or obj.type_obj == "product":
                self.context_object_name = 'product'
                self.template_name = 'catalog/product.html'
            return obj
        except ObjectDoesNotExist:
            raise Http404


class ProductsJSONTemplate(
        JSONResponseMixin, SingleObjectTemplateResponseMixin, TemplateView):
    """
    Страница со всеми товарами. Если был Ajax запрос будет возвращен `Json`
    страницы, иначе будет возвращен `HTML` документ.
    """

    template_name = "catalog/products.html"

    def render_to_response(self, context, **response_kwargs):
        request = self.request
        search = request.GET.get('search', '')
        products = Product.objects.filter(active=True, product_child=None)

        products = products.filter(
            Q(Q(title__icontains=search) | Q(code__icontains=search)))

        context['filters'] = Product.get_filters(products)

        # Фильтрация товаров
        products, context['active'] = Product.filter_products(
            products, request)

        # Пагинация и кнопка показать еще
        if products:
            page = int(request.GET.get("page", 1))
            count_products = products.count()

            if int(count_products) <= int(12):
                context['more_button'] = ""
                context['pagination'] = ""
            else:
                pagin = pagination(products, page, 12)
                products = pagin.items
                if int(count_products / page) >= int(12):
                    context['more_button'] = \
                        get_template(
                            "catalog/includes/more.html").render()
                else:
                    context['more_button'] = ''
                context['pagination'] = \
                    get_template("catalog/includes/pagination.html").render(
                        {'PAGIN': pagin})

        # Возвращение данных в виде Json
        if request.is_ajax():
            # Удаление лишних объектов
            del context['view']

            # Сериализация требуемых олбъектов
            context['products'] = ''
            for item in products:
                context['products'] += \
                    '<div class="col-lg-4 col-sm-6 col-12">' \
                    + item.the_product_card() + '</div>'

            return self.render_to_json_response(context, **response_kwargs)

        print(products)
        context['products'] = products
        return super().render_to_response(context, **response_kwargs)


class SearchView(View):
    """Обработчик запроса на поиск товаров"""

    def post(self, request):
        post_data = request.POST
        search = post_data['search']
        response = ''

        products = Product.objects.filter(
            Q(Q(title__icontains=search) | Q(code__icontains=search)))
        
        categorys = Category.objects.filter(title__icontains=search)
    
        collections = Product.objects.filter(type_obj = "collection").filter(
        Q(Q(title__icontains=search) | Q(code__icontains=search)))[:10]

        # Если по запросу нет товаров или услуг, то возвращаем сообщение о том
        # что по данному запросу ничего не найдено
        if not products and not categorys and not collections:
            return JsonResponse({
                'error': True,
                'message': 'Ничего не найдено'
            })
        
        template =  get_template("catalog/includes/search-result.html").render(
                        {'products': products,'categorys':categorys,'collections':collections})

        # for item in products:
        #     response += '<a class="search-form__dropdown-item" \
        #     href="{0}">{1}</a>'.format(
        #         item.get_absolute_url(), item.title)

        return JsonResponse({'template': template})
