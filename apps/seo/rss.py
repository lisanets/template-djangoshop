from django.contrib.syndication.views import Feed
from apps.posts.models import Category, Post
from apps.configuration.models import Settings

class LatestEntries(Feed):
    title = "Новости сайта RED35"
    link = "/"
    description = "Описание"

    def items(self):
        return Post.objects.filter(active=True).order_by("-published_date")[:10]

    def item_title(self, item):
        return item.title
 
    def item_description(self, item):
        return item.description[0:400]