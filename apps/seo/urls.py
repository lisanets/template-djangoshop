from django.conf.urls import url
# from django.contrib.sitemaps.views import sitemap

# from .sitemap import sitemaps
from .views import robots

# from django.conf.urls.defaults import *
from .rss import LatestEntries



urlpatterns = [
    url(r'^robots.txt$', robots, name="seo-robots"),
    # url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
    #     name='django.contrib.sitemaps.views.sitemap'),
    url(r'^feed/$', LatestEntries()),
]
