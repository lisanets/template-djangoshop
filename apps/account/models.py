from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import authenticate, login as django_login

from apps.feedback.utils import template_email_message


class Account(models.Model):
    """
    Модель пользователя личного кабинета.
    """

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="account")
    email = models.EmailField(default="", blank=True, max_length=100)
    name = models.CharField(verbose_name="Имя", blank=True, max_length=100)
    phone = models.CharField(verbose_name="Телефон",
                             blank=True, max_length=100)

    def __str__(self):
        return self.user.username

    def check_password(self, password):
        user = authenticate(username=self.user.username, password=password)
        if user == self.user:
            return True
        return False

    @staticmethod
    def auth(request, user):
        django_login(request, user)
        return user

    @staticmethod
    def auth_with_password(request, login, password):
        user = authenticate(username=login, password=password)
        if user:
            return Account.auth(request, user)
        return False

    @staticmethod
    def register(email):
        user, created = User.objects.get_or_create(username=email, email=email)
        if not created:
            return False
        password = User.objects.make_random_password(10)
        user.set_password(password)
        user.save()
        template_email_message(
            "account/register.html", subject="Регистрация на сайте",
            to=[email, ], data={'user': user, 'password': password})
        return user

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'Пользователи'


@receiver(post_save, sender=User)
def create_account(sender, instance, created, **kwargs):
    """Создание новой модели пользователя личного кабинета.

    Пользователь личного кабинета создается по сигнялу сохранения стандартного
    пользователя Django `django.contrib.auth.models.User`. Пользователь
    личного кабинета будет создан только в том случае если пользователь Django
    на момент сохранения был создан.

    Arguments:
        sender {Model} -- Модель пользователя Django
        instance {DjangoObject} -- Экземпляр пользователя Django
        created {bool} -- Показывает был ли создан пользователь Django
    """

    if created:
        Account.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_account(sender, instance, **kwargs):
    """Сохранение уже существующего пользователя личного кабинета.

    Пользователь личного кабинета сохраняется по сигнялу сохранения
    пользователя Django.

    Arguments:
        sender {Model} -- Модель пользователя Django
        instance {[type]} -- Экземпляр пользователя Django
    """

    instance.account.save()
