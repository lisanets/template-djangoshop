from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.views import View
from django.views.generic import TemplateView
from django.template.loader import get_template

from apps.feedback.utils import template_email_message
from ..models import Order, OrderItem, Cart


class OrderView(TemplateView):
    context_object_name = 'order'
    template_name = 'shop/order.html'


class OrderCreate(View):
    """Представление оформелинея заказа на товары."""

    def post(self, request):
        post_data = request.POST
        error_list = {}

        if post_data['name'] == '':
            error_list['name'] = 'Укажите ФИО'
        if post_data['phone'] == '':
            error_list['phone'] = 'Укажите телефон'
        if post_data['email'] == '':
            error_list['email'] = 'Укажите email'

        if not error_list:
            cart = self.get_cart(request)
            account = self.get_account(cart)

            order = Order.objects.create(
                account=account, name=post_data['name'],
                phone=post_data['phone'], email=post_data['email'])
            for item in cart.items():
                OrderItem.objects.create(
                    order=order, product=item.product, count=item.count,
                    total=float(item.price()) * int(item.count))
            order.total = float(cart.total())
            order.save()

            self.clear_cart(request, cart)

            template_email_message(
                'shop/order-mail.html', subject="Ваш заказ №" + str(order.id),
                to=[post_data['email']], data={
                    'order': order, 'request': request})

            return JsonResponse({
                'errors': False, 'count': 0, 'template': get_template(
                    template_name='shop/includes/order-success.html').render({
                        'order': order})})
        else:
            return JsonResponse({'errors': True, 'fields': error_list})

    def clear_cart(self, request, cart):
        """Очистка корзины товаров

        Arguments:
            request {object} -- Запрос на сервер
            cart {Cart|UnauthCart} -- Корзина товаров
        """

        if request.user.is_authenticated():
            cart.items().delete()
        else:
            del request.session['cart']

    def get_cart(self, request):
        """Получить корзину товаров

        Arguments:
            request {object} -- Запрос на сервер
        """

        try:
            if request.user.is_authenticated():
                try:
                    return Cart.objects.get(account__user=request.user)
                except ObjectDoesNotExist:
                    raise 'Корзина не существует'
            return request.session['cart']
        except KeyError:
            raise 'Корзина не существует'

    def get_account(self, cart):
        """Получить пользователя личного кабинета.

        Если пользователь `не найден`, то возвращается `None`.

        Arguments:
            cart {Cart|UnauthCart} -- [description]

        Returns:
            Account|None -- Пользователь личного кабинета
        """

        try:
            return cart.account
        except AttributeError:
            return None
