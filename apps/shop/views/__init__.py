from ..views.cart import (
    CartTemplate, CartAddView, CartUpdateView, CartDeleteView
)
from ..views.order import OrderView, OrderCreate
from ..views.favorites import (
    FavoritesView, AddFavoritesView, DeleteFavoritesView, RestoreFavoritesView,
    AddWaitingFavoritesView
)
