from django import forms
from django.contrib import admin
from ajax_select.fields import AutoCompleteSelectField

from .models import Order, OrderItem


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    exclude = ('product',)
    readonly_fields = ('product', 'color')


class OrderForm(forms.ModelForm):
    account = AutoCompleteSelectField('account', required=False,
                                      label="Профиль")


class OrderAdmin(admin.ModelAdmin):
    form = OrderForm
    inlines = [OrderItemInline]


admin.site.register(Order, OrderAdmin)
