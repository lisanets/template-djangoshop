from django.db import models
from django.utils.timezone import datetime

from apps.account.models import Account
from apps.catalog.models import Product, Color


class Order(models.Model):
    """
    Модель заказа товаров
    """

    # Стадии заказа
    STATUSES = (
        ('processing', 'В обработке'),
        ('completed', 'Выполнен'),
        ('canceled', 'Отменен')
    )

    account = models.ForeignKey(
        Account, verbose_name="Профиль",
        blank=True, null=True, related_name="orders")
    name = models.CharField(
        verbose_name="Имя", default="", blank=True, max_length=300)
    phone = models.CharField(verbose_name="Телефон",
                             default="", blank=False, max_length=300)
    email = models.EmailField(verbose_name="Email", default="", blank=False)
    comment = models.TextField(
        verbose_name="Комментарий", blank=True, max_length=300)
    date = models.DateTimeField(
        verbose_name="Дата заказа", default=datetime.now)
    total = models.FloatField(
        verbose_name="Общая стоимость заказа", null=True, blank=True)
    status = models.CharField(verbose_name="Статус", choices=STATUSES,
                              default="processing", max_length=50)

    def __str__(self):
        return "Заказ № " + str(self.id)

    def status_value(self):
        """Получаем значение статуса заказа относительно его ключа.

        Returns:
            str -- Значение статуса заказа
        """

        return dict(self.STATUSES)[self.status]

    class Meta:
        verbose_name = 'заказ'
        verbose_name_plural = 'Заказы'


class OrderItem(models.Model):
    """
    Модель элемента заказа.
    """

    order = models.ForeignKey(Order, verbose_name="Заказ",
                              related_name="order_items")
    product = models.ForeignKey(Product, verbose_name="Товар", null=True,
                                blank=True, related_name="product_order_items",
                                on_delete=models.SET_NULL)
    color = models.ForeignKey(Color, verbose_name="Опция", null=True,
                              blank=True, related_name="color_order_items",
                              on_delete=models.SET_NULL)
    count = models.IntegerField(verbose_name="Количество", default=0)
    total = models.FloatField(verbose_name="Цена на момент покупки",
                              blank=True, default=0)

    def save(self, *args, **kwargs):
        # Присвоение цены на момент покупки. Цена присвивается относительно
        # того, есть ли у элемента заказа вариация цвета.
        if not self.id:
            if self.color:
                self.total = self.color.price
            else:
                self.total = self.product.price
        super(OrderItem, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.order)

    class Meta:
        verbose_name = 'элемент заказа'
        verbose_name_plural = 'Элементы заказа'
