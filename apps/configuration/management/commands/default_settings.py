import os

from django.core.management.base import BaseCommand

from system import settings
from ...models import Settings


class Command(BaseCommand):
    help = 'Получаем дерево каталога, со списком товаров'
    # seo_text = open(os.path.join(settings.BASE_DIR, 'apps', 'configuration',
    #                              'management', 'commands', 'seo-text.html'))
    # privacy_policy = open(os.path.join(settings.BASE_DIR, 'apps',
    #                                    'configuration', 'management',
    #                                    'commands', 'privacy-policy.html'))

    def handle(self, *args, **options):
        Settings.objects.update_or_create(
            language=settings.LANGUAGES[0][0],
            defaults=({
                'language': settings.LANGUAGES[0][0],
                'name': 'RED35',
                'email': 'red35@msil.ru',
                'full_address': 'Котлас, пр. Мира, д. 18 (розничный отдел)',
                'address': 'Котлас, пр. Мира, д. 18',
                'phones': '+7 (921) 070-93-36',
                'time_work': 'Ежедневно с 10:00 до 21:00',
                'coord_x': '61.236809',
                'coord_y': '46.641949',
                'vkontakte': 'vk.com/profikosmetiks',
                # 'seo_text': str(self.seo_text.read()),
                # 'privacy_policy': str(self.privacy_policy.read())
            })
        )

        print('\n\n------------------------------')
        print('Стандартные настройки созданы')
        print('------------------------------\n\n')

