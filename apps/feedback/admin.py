from django.contrib import admin

from .models import Email, Subscriber


admin.site.register(Email)
admin.site.register(Subscriber)
