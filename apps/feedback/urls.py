from django.conf.urls import url

from .views import SubscribeView, QuestionView, OrderServiceView


urlpatterns = [
    # Обработка запроса на добавление товара в конзину
    url(r'^api/feedback/subscribe/$',
        SubscribeView.as_view(),
        name='feedback-subscribe'),
    url(r'^api/feedback/question/$',
        QuestionView.as_view(),
        name='feedback-question'),
    url(r'^api/feedback/order-service/$',
        OrderServiceView.as_view(),
        name='order-service'),
]
