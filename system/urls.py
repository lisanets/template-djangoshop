from django.contrib import admin
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.views.generic import RedirectView

from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import Sitemap
from apps.catalog.models import ProductSitemap, CategorySitemap
from apps.posts.models import SitemapCatPage, SitemapPost
from apps.pages.models import SitemapPage
from django.core.urlresolvers import reverse

#  delete later
from django.views.generic import TemplateView
#


class StaticViewSitemap(Sitemap):

    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return ['catalog',]

    def location(self, item):
        return reverse(item)


sitemaps = {
    'products': ProductSitemap,
    'categorys': CategorySitemap,
    'posts': SitemapCatPage,
    'catsposts': SitemapPost,
    'pages': SitemapPage,
    'static': StaticViewSitemap
}
    
urlpatterns = [
    url(r'^favicon\.ico$',
        RedirectView.as_view(url='/static/images/favicon.ico'),
        name='favicon'),

    url(r'^admin/', admin.site.urls),

    # Прочие приложения
    url(r'^filer/', include('filer.urls')),
    url(r'^ajax_lookup/', include('ajax_select.urls'), name='ajax_lookup'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    # delete later
    url(r'^dev_page', TemplateView.as_view(
        template_name="dev_page.html"), name='dev_page'),

    # sitemap
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'),

    # Основные приложения
    url(r'', include('apps.seo.urls')),
    url(r'', include('apps.feedback.urls')),
    url(r'', include('apps.catalog.urls')),
    url(r'', include('apps.account.urls')),
    url(r'', include('apps.shop.urls')),
    url(r'', include('apps.pages.urls')),
    url(r'', include('apps.posts.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns