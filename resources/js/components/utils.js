let userAgent = navigator.userAgent

/**
 * Импорт всех файлов
 * @param {array} r 
 */
export function importAll(r) {
	return r.keys().map(r);
}

/**
 * Проверенное событие на iPhone
 */
export let eventClick = (userAgent.match(/iPad/i) || userAgent.match(/iPhone/)) ? "touchstart" : "click";


/**
 * 
 * @param {float} number Ваше значение
 * @param {integer} decimals Количество символов после запятой
 * @param {string} dec_point То что будет межды разрядами
 * @param {string} thousands_sep Разделитель между целой частью и дробной
 */
export function numberFormat(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

/**
 * Исключает повторяющиеся значения
 * @param {array} arr
 */
export function unique(arr) {
    let obj = {};

    for (let i = 0; i < arr.length; i++) {
        let str = arr[i];
        obj[str] = true;
    }

    return Object.keys(obj);
}
