$(".product_amount-button").click(function () {
    var $input = $(this).parents('.product_amount').find('input');
    var val = +$input.val();

    if ($(this).hasClass('minus')) $input.val(val - 1);
    else $input.val(val + 1);

    $(this).parents('.product_amount').find('input').change();
});
$(".product_amount input").on("change", function () {
    var val = +$(this).val();
    var min = $(this).attr('min');
    var max = $(this).attr('max');
    $(this).val((val > max) ? max : (val < min) ? min : val);
});