/**
 * Создание заглушки предзагрузчика заглушки на объекте по селектору
 * @param {str|Jquery<HTMLElement>} selector Селектор объекта поверх которого и будет создан прелоадер
 * @param {int} time Время появления прелоадера
 */
export function createPreloader(selector, time = 150) {
    let $container = $(selector)
    let html =
        `<div class="preloader">
            <div class="preloader__figure preloader__figure_size_m">
                <div class="preloader__item preloader__item-1"></div>
                <div class="preloader__item preloader__item-2"></div>
                <div class="preloader__item preloader__item-3"></div>
                <div class="preloader__item preloader__item-4"></div>
                <div class="preloader__item preloader__item-5"></div>
                <div class="preloader__item preloader__item-6"></div>
                <div class="preloader__item preloader__item-7"></div>
                <div class="preloader__item preloader__item-8"></div>
            </div>
        </div>`

    let position = $container.css('position')
    if (position != 'absolute' && position != 'fixed')
        $container[0].style.position = 'relative'
    $container.append(html)
}

// 
export function deletePreloader(selector, time = 150) {
    let $container = $(selector)

    $container.find('.preloader').fadeIn(time)
    setTimeout(() => {
        $container.find('.preloader').remove()
        $container[0].style.position = ''
    }, time)
}

export function sendSuccess(selector, message, time = 150) {
    let $container = $(selector)
    let html =
        `<div class="send-success">
            <svg class="send-success__icon" role="img">
                <use xlink:href="/static/images/sprite.svg#done_icon"></use>
            </svg>
            <p class="send-success__message">${ message }</p>
        </div>`

    let position = $container.css('position')
    if (position != 'absolute' && position != 'fixed')
        $container[0].style.position = 'relative'
    $container.append(html)
}
