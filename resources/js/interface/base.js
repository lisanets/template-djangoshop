
// Навигация по категориям товаров в шапке
$('.catalog-button').hover((event) => {
    $('.dropdown-catalog').stop(true, false).fadeIn(150);
}, (event) => {
    $('.dropdown-catalog').stop(true, false).fadeOut(150);
})
$('.dropdown-catalog__item').hover((event) => {
    $(event.currentTarget).children('.sub-dropdown').stop(true, false).fadeIn(150).css('display', 'flex');
}, (event) => {
    $(event.currentTarget).children('.sub-dropdown').stop(true, false).fadeOut(150);
})
