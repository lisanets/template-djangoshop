import { createToolpopup, deleteToolpopup } from '../interface/toolpopup';
import { createPreloader, deletePreloader } from '../interface/preloader';


function order(event) {
    event.preventDefault();

    createPreloader('body');

    let form = event.target;
    let data = $(form).serialize();
    let action = form.action;
    let method = form.method;

    $.ajax({
        url: action,
        type: method,
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response);

            deletePreloader('body');

            document.querySelector('.cart-button__count').innerHTML = response.count
            
            if(response.errors) {
                for(let item in response.fields) {
                    let input = form.querySelector(`[name="${ item }"]`);
                    input.classList.add('invalid-field');
                    $(input.parentNode).append(`<span class="input__error">${ response.fields[item] }</span>`)
                }
            } else {
                // Обновление числа количества товаров в корзине
                document.querySelector('.cart-button__count').innerText = response.count;

                let orederWrapper = document.querySelector('.order-wrapper');
                orederWrapper.style.opacity = 1;

                // Анимация изчезания и появления корзины со сменой на шаблон пустой корзины
                $(orederWrapper).animate({
                    opacity: 0
                }, 200, () => {
                    $(orederWrapper).html(response.template);
                    $(orederWrapper).animate({opacity: 1}, 200);
                })   
            }
        },
        error: function(error) {
            console.log(error);
        }
    });
}


$('body').on('submit', '.order-form', order)