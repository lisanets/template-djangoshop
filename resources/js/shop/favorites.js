function addFavorites(event) {
    event.preventDefault()
    let csrf = document.querySelector('.csrf_token input')
    let button = event.currentTarget
    let action = button.dataset.action
    let product = button.dataset.product
    let data = `${ csrf.name }=${ csrf.value }&product=${ product }`

    $.ajax({
        url: action,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            document.querySelector('.favorite-button__count').innerHTML = response.count
            if (button.classList.contains('card-action_added'))
                button.classList.remove('card-action_added')
            else
                button.classList.add('card-action_added')
        },
        error: function(error) {
            console.log(error)
        }
    })
}

function deleteFavorites(event) {
    event.preventDefault()
    let csrf = document.querySelector('.csrf_token input')
    let button = event.currentTarget
    let action = button.dataset.action
    let product = button.dataset.product
    let data = `${ csrf.name }=${ csrf.value }&product=${ product }`

    $.ajax({
        url: action,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            document.querySelector('.favorite-button__count').innerHTML = response.count

            button.style.opacity = '0'
            $(button).siblings('.product-card__body').append(response.restore)
            setTimeout(() => {
                $(button).siblings('.product-card__body').find('.favorites-restore').css('opacity', '1')
            }, 10)
        },
        error: function(error) {
            console.log(error)
        }
    })
}

function restoreFavorites(event) {
    event.preventDefault()
    let csrf = document.querySelector('.csrf_token input')
    let button = event.currentTarget
    let action = button.dataset.action
    let product = button.dataset.product
    let data = `${ csrf.name }=${ csrf.value }&product=${ product }`

    $.ajax({
        url: action,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            document.querySelector('.favorite-button__count').innerHTML = response.count
            button.closest('.product-card').querySelector('.favorites-delete').style.opacity = ''
            button.parentNode.remove()
        },
        error: function(error) {
            console.log(error)
        }
    })
}

function addFavoritesWaiting(event) {
    event.preventDefault()
    let csrf = document.querySelector('.csrf_token input')
    let button = event.currentTarget
    let action = button.dataset.action
    let product = button.dataset.product
    let data = `${ csrf.name }=${ csrf.value }&product=${ product }`

    $.ajax({
        url: action,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function(response) {
            console.log(response)
            document.querySelector('.favorite-button__count').innerHTML = response.count
            button.closest('.product-card').querySelector('.product-card__favorite').classList.add('card-action_added')
            button.classList.add('card-button_replacer')
            $(button).siblings('.card-button_replacer').removeClass('card-button_replacer')
        },
        error: function(error) {
            console.log(error)
        }
    })
}


$('body').on('click', '.favorites-add', addFavorites)
$('body').on('click', '.favorites-delete', deleteFavorites)
$('body').on('click', '.favorites-restore__button', restoreFavorites)
$('body').on('click', '.favorites-waiting-add', addFavoritesWaiting)
