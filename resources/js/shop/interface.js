function openDropDown(event) {
    let button = event.currentTarget;
    let dropDown = button.querySelector('.shop-dropdown');

    if (dropDown !== null) {
        button.classList.add('blocked-ajax-fadeOut')
        dropDown.style.transform = 'translateY(0px)'
        $(dropDown).stop(true, false).fadeIn(150);
        $(window).resize()
    }
}

function closeDropDown(event) {
    let button = event.currentTarget;
    let dropDown = button.querySelector('.shop-dropdown');

    if (dropDown !== null) {
        button.classList.remove('blocked-ajax-fadeOut')
        dropDown.style.transform = ''
        $(dropDown).stop(true, false).fadeOut(150);
    }
}


$('body').on('mouseenter', '.shop-button', openDropDown)
$('body').on('mouseleave', '.shop-button', closeDropDown)
