import { importAll } from '../components/utils'


const shop = importAll(require.context('./', false, /\.(js|jsx)$/));
