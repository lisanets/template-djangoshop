const path = require('path');
const webpack = require('webpack')
const BabiliPlugin = require("babili-webpack-plugin");

const paths = {
    resources: {
        self:      __dirname + '/resources/',
        js:        __dirname + '/resources/js'
    },
    assets: {
        self:      __dirname + '/static',
        js:        __dirname + '/static/js'
    },
}

module.exports = {
    entry: {
        vendor: [paths.resources.js + '/vendors.js']
    },
    output: {
        filename: '[name].bundle.js',
        path: paths.assets.js,
        library: '[name]_lib'
    },
    plugins: [
        new BabiliPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            _: 'lodash',
            anime: 'animejs',
        }),
        new webpack.DllPlugin({
            path: paths.assets.js + '/[name]-manifest.json',
            name: '[name]_lib'
        })
    ]
}
